jQuery(function ($) {
  function niceAccordion(field) {
    const title = field.$el.closest('[data-block]').data('title');
    const {
      template_directory_uri,
      render_template
    } = acf.data.blockTypes.filter(block => block.title === title)[0];
    const imageUrl = render_template.replace('index.php', 'screenshot.png');
    field.$el.find('label').first().text(title)
    field.$el.find('.acf-accordion-title').first()
      .css('background-image', `url(${template_directory_uri}/${imageUrl})`)
    console.log('ee')

    if (field.$el.find('.acf-image-select')) {
      const choicesImages = field.$el.find('.acf-image-select').find('img');
      if (choicesImages) {
        for (let choicesImage of choicesImages) {
          const imgSrc = choicesImage.src;
          if (imgSrc.includes('$a7a$')) {
            const realImgSrc = imgSrc.substring(imgSrc.indexOf('$a7a$') + 5);
            const base_url = window.location.origin;
            const pathArray = window.location.pathname.split('/');
            const ourDomain = base_url + '/' + pathArray[1];
            const clientDomain = base_url;
            const host = window.location.host;
            if (host === 'localhost' || host === 'codesign82.com') {
              choicesImage.src = ourDomain + template_directory_uri.replace(ourDomain, '') + '/acf-images' + realImgSrc;
            } else {
              choicesImage.src = clientDomain + template_directory_uri.replace(clientDomain, '') + '/acf-images' + realImgSrc;
            }
          }
        }
      }
    }
  }

  acf.addAction('load_field/key=field_606707efda9a8', niceAccordion)
  acf.addAction('append_field/key=field_606707efda9a8', niceAccordion)

});
