<?php
get_header();

$posts_page_id = get_option( 'page_for_posts' );
$is_tax_page    = is_tax() || is_category();
$posts_per_page = $is_tax_page ? get_field( 'tax_posts_per_page', 'options' ) :
  get_field( 'posts_per_page', $posts_page_id );
?>
<div class="posts-page-wrapper"
     data-cat-name="<?= single_term_title( '', false ) ?>"
     data-tax="<?= $is_tax_page ? 'true' : 'false' ?>">

<?php
get_footer();
