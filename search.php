<?php
/*
 * Template Name: Search Page
 * */
get_header();
?>

  <section data-section-class="search-page-wrapper" class="search-page-wrapper">
    <div class="container">
      <?php get_search_form(); ?>
      <?php if ( have_posts() && @$_GET['s'] != '' ) : ?>
        <h1 class="headline-1">
          <?php _e( 'Search results for: ', 'zineOne' ); ?>
          <p class="paragraph"><?php echo get_search_query(); ?></p>
        </h1>
      <?php endif; ?>
      <div class="row row-cols-1 row-cols-md-3">
        <?php if ( have_posts() && @$_GET['s'] != '' ) : ?>

          <?php
          // Start the Loop.
          while ( have_posts() ) :
            the_post();
            get_template_part( "template-parts/card" );
            // End the loop.
          endwhile;
        endif;
        if ( ! have_posts() ):
          echo '<h2 class="headline-1">there is no results :(</h2>';
        endif;
        ?>
      </div>
    </div>
  </section>

<?php
get_footer();
