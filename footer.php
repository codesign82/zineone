<?php wp_footer(); ?>
<?php
$footer_logo = get_field( 'footer_logo', 'options' );
// Prefooter
?>
<!--region footer-->
<footer>
  <div class="container">
    <div class="footer">
      <div class="footer-group">
        <picture class="footer-logo">
          <img src="<?= get_template_directory_uri() . '/front-end/src/images/zineone-logo-3%201.png' ?>" alt="">
        </picture>
        <div class="selects-wrapper">
          <div class="wrapper-selector">
            <a href="#"> Why ZineOne</a>
          </div>
          <div class="wrapper-selector">
            <a href="#"> Industries</a>
          </div>
          <div class="wrapper-selector">
            <a href="#"> Capabilities</a>
          </div>
        </div>
      </div>
      <div class="zineOne-details">
        <div class="footer-contact">
          <p class="paragraph">Silicon Valley, CA</p>
          <p class="paragraph">Info@ZineOne.com</p>
          <p class="paragraph">(844) 946-3663</p>
        </div>
        <div class="footer-icons">
          <div class="footer-icon">
            <a href="#">
              <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                   xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M20 10.0609C20 4.50304 15.5242 0 10 0C4.47581 0 0 4.50304 0 10.0609C0 15.0913 3.62903 19.2698 8.42742 20V12.9817H5.8871V10.0609H8.42742V7.87018C8.42742 5.35497 9.91935 3.93509 12.1774 3.93509C13.3065 3.93509 14.4355 4.13793 14.4355 4.13793V6.61258H13.1855C11.9355 6.61258 11.5323 7.38337 11.5323 8.19473V10.0609H14.3145L13.871 12.9817H11.5323V20C16.3306 19.2698 20 15.0913 20 10.0609Z"
                  fill="#A0CD37"/>
              </svg>
            </a>

          </div>
          <div class="footer-icon">
            <a href="#">
              <svg width="18" height="18" viewBox="0 0 18 18" fill="none"
                   xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M16.7143 0H1.24554C0.5625 0 0 0.602679 0 1.32589V16.7143C0 17.4375 0.5625 18 1.24554 18H16.7143C17.3973 18 18 17.4375 18 16.7143V1.32589C18 0.602679 17.3973 0 16.7143 0ZM5.42411 15.4286H2.77232V6.87054H5.42411V15.4286ZM4.09821 5.66518C3.21429 5.66518 2.53125 4.98214 2.53125 4.13839C2.53125 3.29464 3.21429 2.57143 4.09821 2.57143C4.94196 2.57143 5.625 3.29464 5.625 4.13839C5.625 4.98214 4.94196 5.66518 4.09821 5.66518ZM15.4286 15.4286H12.7366V11.25C12.7366 10.2857 12.7366 9 11.3705 9C9.96429 9 9.76339 10.0848 9.76339 11.2098V15.4286H7.11161V6.87054H9.64286V8.03572H9.68304C10.0446 7.35268 10.9286 6.62946 12.2143 6.62946C14.9063 6.62946 15.4286 8.4375 15.4286 10.7277V15.4286Z"
                  fill="#A0CD37"/>
              </svg>
            </a>

          </div>
          <div class="footer-icon">
            <a href="#">
              <svg width="20" height="16" viewBox="0 0 20 16" fill="none"
                   xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M17.9648 3.97108C18.7476 3.39277 19.4521 2.6988 20 1.88916C19.2955 2.19759 18.4736 2.42892 17.6517 2.50602C18.5127 2.00482 19.1389 1.23373 19.4521 0.26988C18.6693 0.73253 17.7691 1.07952 16.8689 1.27229C16.0861 0.462651 15.0294 0 13.8552 0C11.5851 0 9.7456 1.81205 9.7456 4.04819C9.7456 4.35663 9.78474 4.66506 9.86301 4.97349C6.45793 4.78072 3.40509 3.16145 1.36986 0.73253C1.01761 1.31084 0.821918 2.00482 0.821918 2.7759C0.821918 4.16386 1.52642 5.39759 2.66145 6.13012C1.99609 6.09157 1.33072 5.93735 0.782779 5.62892V5.66747C0.782779 7.63373 2.19178 9.25301 4.07045 9.63855C3.75734 9.71566 3.36595 9.79277 3.0137 9.79277C2.73973 9.79277 2.50489 9.75422 2.23092 9.71566C2.73973 11.3349 4.26614 12.4916 6.06654 12.5301C4.65753 13.6096 2.89628 14.2651 0.978474 14.2651C0.626223 14.2651 0.313112 14.2265 0 14.188C1.80039 15.3446 3.95303 16 6.30137 16C13.8552 16 17.9648 9.86988 17.9648 4.51084C17.9648 4.31807 17.9648 4.16386 17.9648 3.97108Z"
                  fill="#A0CD37"/>
              </svg>
            </a>
          </div>
          <div class="footer-icon">
            <a href="#">
              <svg class="footer-icon" width="22" height="16" viewBox="0 0 22 16" fill="none"
                   xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M21.5174 2.54167C21.2761 1.54167 20.5119 0.75 19.5868 0.5C17.8574 0 11.0201 0 11.0201 0C11.0201 0 4.1426 0 2.41316 0.5C1.48812 0.75 0.723949 1.54167 0.482633 2.54167C0 4.29167 0 8.04167 0 8.04167C0 8.04167 0 11.75 0.482633 13.5417C0.723949 14.5417 1.48812 15.2917 2.41316 15.5417C4.1426 16 11.0201 16 11.0201 16C11.0201 16 17.8574 16 19.5868 15.5417C20.5119 15.2917 21.2761 14.5417 21.5174 13.5417C22 11.75 22 8.04167 22 8.04167C22 8.04167 22 4.29167 21.5174 2.54167ZM8.76783 11.4167V4.66667L14.479 8.04167L8.76783 11.4167Z"
                  fill="#A0CD37"/>
              </svg>
            </a>
          </div>
        </div>
      </div>
      <hr>
      <div class="footer-text">
        <p class="paragraph">2021 © Copyright All Rights Reserved</p>
        <p class="paragraph">Terms & Policy</p>
      </div>
    </div>
  </div>
</footer>
<!--endregion footer-->
<!--</div><!--end of page-transition -->
<!--</main>-->
<script>
  <!--  --><?php //$href = get_template_directory_uri() . '/assets/main.css'; ?>
  //  const link = document.createElement('link');
  //  link.href = '<?//=$href?>//';
  //  link.type = 'text/css';
  //  link.media = 'all';
  //  link.rel = 'stylesheet';
  //  setTimeout(() => {
  //    document.body.appendChild(link);
  //  }, 3000)
</script>
<script>
  <!--  --><?php //$url = get_template_directory_uri() . '/assets/main.js'; ?>
  //  const script = document.createElement('script');
  //  script.src = '<?//=$url?>//';
  //  <?php //if(is_user_logged_in()){ ?>
  //  setTimeout(() => {
  //    document.body.appendChild(script);
  //  }, 100)
  //  <?php //}else{ ?>
  //  setTimeout(() => {
  //    document.body.appendChild(script);
  //  }, 4000)
  //  <?php //} ?>
</script>
<script>
  <!--  --><?php //$href_quform = site_url() . '/wp-content/plugins/quform/cache/quform.css'; ?>
  //  const link_quform = document.createElement('link');
  //  link_quform.href = '<?//=$href_quform?>//';
  //  link_quform.type = 'text/css';
  //  link_quform.media = 'all';
  //  link_quform.rel = 'stylesheet';
  //  setTimeout(() => {
  //    document.body.appendChild(link_quform);
  //  }, 4000)
</script>
<script>
  <!--  --><?php //$url_jquery = site_url() . '/wp-includes/js/jquery/jquery.min.js'; ?>
  //  const script_jquery = document.createElement('script');
  //  script_jquery.src = '<?//=$url_jquery?>//';
  //  setTimeout(() => {
  //    document.body.appendChild(script_jquery);
  //  }, 5000)
  //  <?php //$url_quform = site_url() . '/wp-content/plugins/quform/cache/quform.js'; ?>
  //  const script_quform = document.createElement('script');
  //  script_quform.src = '<?//=$url_quform?>//';
  //  setTimeout(() => {
  //    document.body.appendChild(script_quform);
  //  }, 5300)
</script>
<div class="video-modal custom-modal" id="custom-modal">
  <div class="custom-modal-inner">
    <button class="close-modal" aria-label="Close Modal">
      <svg height="16" viewBox="0 0 17 16" width="17" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.274 1.676L14.6 0l-6.19 6.19L2.22 0 .545 1.676l6.19 6.189-6.19 6.19L2.22 15.73l6.19-6.19 6.189 6.19 1.675-1.676-6.189-6.189z" fill="#9899a2"/>
      </svg>
    </button>
    <div class="custom-modal-wrap">
      <div class="video-wrapper">
        <iframe allowfullscreen src></iframe>
      </div>
    </div>
  </div>
</div>
</body>
</html>
