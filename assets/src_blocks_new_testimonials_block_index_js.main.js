/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(self["webpackChunkzineOne"] = self["webpackChunkzineOne"] || []).push([["src_blocks_new_testimonials_block_index_js"],{

/***/ "./src/blocks/new_testimonials_block/index.js":
/*!****************************************************!*\
  !*** ./src/blocks/new_testimonials_block/index.js ***!
  \****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string.js */ \"./node_modules/core-js/modules/es.object.to-string.js\");\n/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise.js */ \"./node_modules/core-js/modules/es.promise.js\");\n/* harmony import */ var core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var regenerator_runtime_runtime_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! regenerator-runtime/runtime.js */ \"./node_modules/regenerator-runtime/runtime.js\");\n/* harmony import */ var regenerator_runtime_runtime_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(regenerator_runtime_runtime_js__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _index_html__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.html */ \"./src/blocks/new_testimonials_block/index.html\");\n/* harmony import */ var _index_html__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_index_html__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./style.scss */ \"./src/blocks/new_testimonials_block/style.scss\");\n/* harmony import */ var _scripts_functions_imageLazyLoading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../scripts/functions/imageLazyLoading */ \"./src/scripts/functions/imageLazyLoading.js\");\n/* harmony import */ var _scripts_general_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../scripts/general/animations */ \"./src/scripts/general/animations/index.js\");\n/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! swiper */ \"./node_modules/swiper/swiper.esm.js\");\n\n\n\n\nfunction asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }\n\nfunction _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"next\", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"throw\", err); } _next(undefined); }); }; }\n\n\n\n\n\n\n/**\r\n *\r\n * @param block {HTMLElement}\r\n * @returns {Promise<void>}\r\n */\n\nvar newTestimonialsBlock = /*#__PURE__*/function () {\n  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(block) {\n    var mySwiper, enableSwiper;\n    return regeneratorRuntime.wrap(function _callee$(_context) {\n      while (1) {\n        switch (_context.prev = _context.next) {\n          case 0:\n            // add block code here\n            enableSwiper = function enableSwiper() {\n              mySwiper = new swiper__WEBPACK_IMPORTED_MODULE_7__.default(block.querySelector('.mySwiper'), {\n                // loop: true,\n                slidesPerView: 1,\n                spaceBetween: 20,\n                // slidesPerView: 'auto'\n                grabCursor: true,\n                pagination: true,\n                navigation: {\n                  nextEl: \".swiper-button-next\",\n                  prevEl: \".swiper-button-prev\"\n                },\n                breakpoints: {\n                  600: {\n                    slidesPerView: 1\n                  }\n                }\n              });\n            };\n\n            enableSwiper();\n            (0,_scripts_general_animations__WEBPACK_IMPORTED_MODULE_6__.animations)(block);\n            (0,_scripts_functions_imageLazyLoading__WEBPACK_IMPORTED_MODULE_5__.imageLazyLoading)(block);\n\n          case 4:\n          case \"end\":\n            return _context.stop();\n        }\n      }\n    }, _callee);\n  }));\n\n  return function newTestimonialsBlock(_x) {\n    return _ref.apply(this, arguments);\n  };\n}();\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (newTestimonialsBlock);\n\n//# sourceURL=webpack://zineOne/./src/blocks/new_testimonials_block/index.js?");

/***/ }),

/***/ "./src/blocks/new_testimonials_block/index.html":
/*!******************************************************!*\
  !*** ./src/blocks/new_testimonials_block/index.html ***!
  \******************************************************/
/***/ (function() {

eval("\n\n//# sourceURL=webpack://zineOne/./src/blocks/new_testimonials_block/index.html?");

/***/ }),

/***/ "./src/blocks/new_testimonials_block/style.scss":
/*!******************************************************!*\
  !*** ./src/blocks/new_testimonials_block/style.scss ***!
  \******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack://zineOne/./src/blocks/new_testimonials_block/style.scss?");

/***/ })

}]);