import './styles/style.scss';
import {gsap} from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';
import header from './blocks/header_block';
import hero from './blocks/hero_block';
import {initBlocks} from './blocks';
import {breakLine} from './scripts/functions/breakLine';
import {getHeightOfViewPort} from './scripts/functions/getHeightOfViewPort';
import {setFormFunctionality} from './scripts/general/set-form-functionality';
import {generateDataHover} from './scripts/functions/generateDataHover';
import {scrollToHash} from './scripts/general/scroll-to-hash';
import {initModal} from "./scripts/general/custom-modal";

const reInvokableFunction = async (container = document) => {
  container.querySelector('header') && await header(container.querySelector('header'));
  container.querySelector('.hero_block') && await hero(container.querySelector('.hero_block'));

  await initBlocks(container);

  setFormFunctionality(container);
  generateDataHover(container);
  scrollToHash(container);
  // breakLine(container);
  getHeightOfViewPort(container);

  ScrollTrigger.refresh(false);
};
let loaded = false;

async function onLoad() {
  gsap.config({
    nullTargetWarn: false,
  });
  if (document.readyState === 'complete' && !loaded) {
    loaded = true;
    initModal();
    gsap.registerPlugin(ScrollTrigger);

    await reInvokableFunction();
    document.body.classList.add('loaded');
  }
}

onLoad();

document.onreadystatechange = function () {
  onLoad();
};
