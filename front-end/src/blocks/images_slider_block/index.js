import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import Swiper, {Navigation} from "swiper";

Swiper.use([Navigation])
const imagesSliderBlock = async (block) => {
  new Swiper(block.querySelector('.swiper-container'), {
    slidesPerView: 1,
    spaceBetween: 30,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });


  animations(block);
  imageLazyLoading(block);
};

export default imagesSliderBlock;

