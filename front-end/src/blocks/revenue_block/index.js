import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {gsap} from 'gsap';
import DrawSVGPlugin from "gsap/DrawSVGPlugin";
import {ScrollTrigger} from "gsap/ScrollTrigger";
gsap.registerPlugin(DrawSVGPlugin, ScrollTrigger)

const revenueBlock = async (block) => {

  document.body.addEventListener('mousemove', eyeball);
  const eyeBall = block.querySelector(".eye-ball");
  const eyePupil = block.querySelector(".eye-wrapper-circle");
  const eyeLid = block.querySelector("#eye-lid");

  function eyeball(event) {
    const x = (eyePupil.getBoundingClientRect().left) + (eyePupil.clientWidth / 2);
    const y = (eyePupil.getBoundingClientRect().top) + (eyePupil.clientHeight / 2);
    const radian = Math.atan2(event.pageX - x, event.pageY - y);
    const rot = (radian * (180 / Math.PI) * -1) + 270;
    eyePupil.style.transform = "rotate(" + rot + "deg)";
  }

  const eyeBlinkTl = gsap.timeline({
    yoyo: true,
    repeat: 1,

    defaults: {ease: 'power3.in', duration: 0.2}
  })
    .to(eyeBall, {scaleY: 0})
    .to(eyeLid.children, {attr: {d: 'M5 32.41 S61 32.41 ,116.8 32.41'}}, '<')

  setInterval(() => eyeBlinkTl.play(0), 3000)


  gsap.from(block.querySelector(".counter"), {
    textContent:0 + "%",
    duration:5,
    scrollTrigger: {
      trigger:block,
      start:"top 90%"
    },
    snap: { textContent: 1 }
  })

const timer = gsap.timeline()
  .from(block.querySelector(".circle-wrapper"), {
    drawSVG:0,
    duration:1
  })
  .from(block.querySelector("#counter-shape polyline"), {
    drawSVG:0,
    duration:1
  },"<50%")
  .from(block.querySelector(".small-circle"), {
    scale:0,
    transformOrigin:"center",
    duration:1
  },"<50%")
  .from(block.querySelector(".count-arrow"), {
    drawSVG:0,
    rotate:360,
    transformOrigin:"bottom left",
    duration:1.3
  },"<50%")
  .from(block.querySelector(".big-progress"), {
    drawSVG:0,
    duration:1
  },"<10%")
  .from(block.querySelector(".small-progress"), {
    drawSVG:0,
    duration:1
  },"<10%")
  .to(block.querySelector(".count-arrow"), {
    rotate:360,
    transformOrigin:"bottom left",
    duration:2,
    yoyo:true,
    repeat:-1,
    repeatDelay:2,
    ease:"power2.in"
  })
  .to(block.querySelector(".big-progress"), {
    drawSVG:0,
    duration:2,
    yoyo:true,
    repeat:-1,
    repeatDelay:2,
    ease:"power2.in"
  },"<")
  .to(block.querySelector(".small-progress"), {
    drawSVG:0,
    duration:2,
    yoyo:true,
    repeat:-1,
    repeatDelay:2,
    ease:"power2.in"
  },"<")


  animations(block);
  imageLazyLoading(block);
};

export default revenueBlock;

