import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import Swiper from "swiper";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const newTestimonialsBlock = async (block) => {

  // add block code here
  let mySwiper;
  const enableSwiper = function () {

    mySwiper = new Swiper(block.querySelector('.mySwiper'), {
      // loop: true,
      slidesPerView: 1,
      spaceBetween: 20,
      // slidesPerView: 'auto'
      grabCursor: true,
      pagination: true,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      breakpoints: {
        600: {
          slidesPerView: 1,

        },

      },

    });
  };
  enableSwiper()

  animations(block);
  imageLazyLoading(block);
};

export default newTestimonialsBlock;

