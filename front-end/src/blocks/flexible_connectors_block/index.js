import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import Swiper from "swiper";

const flexibleConnectorsBlock = async (block) => {

  (function () {

    'use strict';
    const breakpoint = window.matchMedia('(min-width:992px)');
    let mySwiper;

    const breakpointChecker = function () {
      if (breakpoint.matches === true) {

        if (mySwiper !== undefined) mySwiper.destroy(true, true);

        return;

        // else if a small viewport and single column layout needed
      } else if (breakpoint.matches === false) {

        // fire small viewport version of swiper
        return enableSwiper();

      }

    };

    const enableSwiper = function () {

      mySwiper = new Swiper('.swiper-container', {

        loop: true,
        slidesPerView: 1.3,
        spaceBetween: 20,
        // slidesPerView: 'auto'
        grabCursor: true,
        breakpoints: {
          600: {
            slidesPerView: 3,

          },

        }

      });

    };

    breakpoint.addListener(breakpointChecker);

    breakpointChecker();


  })();

  animations(block);
  imageLazyLoading(block);
};

export default flexibleConnectorsBlock;

