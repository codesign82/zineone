import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {confettiFall} from "../../scripts/general/animations/confettiFall";


const requestDemoPopupBlock = async (block) => {

  // add block code here


    animations(block);
    imageLazyLoading(block);
    confettiFall(block);
};

export default requestDemoPopupBlock;

