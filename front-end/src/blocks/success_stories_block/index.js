import './index.html';
import './style.scss';
import Swiper from "swiper";
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";


const successStoriesBlock = async (block) => {

  new Swiper('.swiper-container', {
    slidesPerView: 1.1,
    spaceBetween: 20,
    centeredSlides:true,
    breakpoints: {
      // when window width is >= 320px
      600: {
        centeredSlides:false,
        slidesPerView: 1.5299,
      },
   }
  });

  animations(block);

  imageLazyLoading(block);
};

export default successStoriesBlock;

