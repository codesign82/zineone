import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";


const newSeeActionBlock = async (block) => {
  const videoWrapper = block.querySelector(".video-wrapper");
  const video = block.querySelector(".video");
  const playerIcon = block.querySelector(".svg-wrapper svg");

  videoWrapper.addEventListener("click", videoToggling)

  function videoToggling() {
    if (video.paused) {
      video.play()
      playerIcon.style.opacity = "0";
    } else {
      video.pause()
      playerIcon.style.opacity = "1";
    }
  }

  animations(block);
  imageLazyLoading(block);
};

export default newSeeActionBlock;

