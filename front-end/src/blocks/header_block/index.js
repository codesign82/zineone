import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../scripts/functions/imageLazyLoading';
import {animations} from '../../scripts/general/animations';
import {gsap} from "gsap";

export default async (header) => {

  // region check of megamenu out of window

  const isOutOfViewport = function (elem) {
    // Get element's bounding
    const bounding = elem.getBoundingClientRect();
    // Check if it's out of the viewport on each side
    const out = {};
    out.left = bounding.left < 0;
    out.right = bounding.right > (window.innerWidth || document.documentElement.clientWidth);
    if (out.left) {
      elem.classList.add('megamenu-outside-left')
    } else if (out.right) {
      elem.classList.add('megamenu-outside-right')
    }
    return out;

  };
  const megamenus = header.querySelectorAll('.megamenu');
  for (let megamenu of megamenus) {
    isOutOfViewport(megamenu)
  }

  // endregion check of megamenu out of window

  // hover on links has megamenu

  const headerLinksWrapper = header.querySelector('.header-links');
  const headerLinks = header.querySelectorAll('.header-link.has-megamenu');

  // menu links toggler

  const burgerMenu = header.querySelector('.burger-menu');
  const menuLinks = header.querySelector('.header-links');


  if (!burgerMenu) return;
  const burgerTl = gsap.timeline({paused: true});
  const burgerSpans = burgerMenu.querySelectorAll('span');
  gsap.set(burgerSpans, {transformOrigin: 'center'});
  burgerTl
    .to(burgerSpans, {
      y: gsap.utils.wrap([`0.6rem`, 0, `-0.6rem`]),
      duration: 0.25,
    })
    .set(burgerSpans, {autoAlpha: gsap.utils.wrap([1, 0, 1])})
    .to(burgerSpans, {rotation: gsap.utils.wrap([45, 0, -45])})
    .set(burgerSpans, {rotation: gsap.utils.wrap([45, 0, 135])});
  burgerMenu.addEventListener('click', function () {
    if (burgerMenu.classList.contains('burger-menu-active')) {
      // allowPageScroll()
      burgerMenu.classList.remove('burger-menu-active');
      menuLinks.classList.remove('header-links-active');
      header.classList.remove('header-active');
      burgerTl.reverse();
    } else {
      burgerMenu.classList.add('burger-menu-active');
      menuLinks.classList.add('header-links-active');
      header.classList.add('header-active');
      burgerTl.play();
      // preventPageScroll();
      gsap.fromTo([menuLinks.querySelectorAll('.header-link '), menuLinks.querySelectorAll('.btn')], {
        y: 30,
        autoAlpha: 0,
      }, {
        y: 0,
        autoAlpha: 1,
        stagger: .05,
        duration: .4,
        delay: .5,
      });
    }
  });


  // region open sub menu in responsive
  const mobileMedia = window.matchMedia('(max-width: 992px)');
  headerLinks.forEach((menuItem) => {
    const menuItemBody = menuItem.querySelector('.megamenu');
    menuItem?.addEventListener('click', (e) => {
      if (!mobileMedia.matches) return;
      if (!menuItemBody) {
        return;
      }
      const isOpened = menuItem?.classList.toggle('active-header-link');
      if (!isOpened) {
        gsap.to(menuItemBody, {height: 0});
      } else {
        gsap.to(Array.from(headerLinks).map(otherMenuItem => {
          const otherMenuItemBody = otherMenuItem.querySelector('.megamenu');
          if (otherMenuItemBody && menuItem !== otherMenuItem) {
            otherMenuItem?.classList.remove('active-header-link');
            gsap.set(otherMenuItem, {zIndex: 1});
          }
          return otherMenuItemBody;
        }), {height: 0});
        gsap.set(menuItem, {zIndex: 2});
        gsap.to(menuItemBody, {height: 'auto'});
      }
    });
  });
  // endregion open sub menu in responsive

  // header sticky



  // megamenu slider video
  // const slides = header.querySelectorAll('.header-link.has-megamenu .megamenu .megamenu-slide-has-video');
  // for (let slide of slides) {
  //   const videoWrapper = slide.querySelector('.video-wrapper');
  //   const toggler = slide.querySelector('.megamenu-slide-has-video .open-video-modal');
  //   customModal(toggler, videoWrapper);
  // }

  animations(header);
  imageLazyLoading(header);
};

