import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import Swiper from "swiper";


const ourTeamCardsBlock = async (block) => {

  const swiper = new Swiper('.swiper-container', {
    slidesPerView: 'auto',
    spaceBetween: 20,

  });


    animations(block);
    imageLazyLoading(block);
};

export default ourTeamCardsBlock;

