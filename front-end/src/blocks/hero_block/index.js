import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import gsap from "gsap"

const heroBlock = async (block) => {
  // const btn = block.querySelector("#play-btn");
  // btn?.addEventListener('click', () => {
  //   if (!btn.classList.contains('playing')) {
  //     btn.classList.add('playing')
  //   } else {
  //     btn.classList.remove('playing')
  //   }
  // });
  // const videoToggler = block.querySelector('.video-toggler');
  // const video = block.querySelector('video');
  // console.log(videoToggler)
  // videoToggler.addEventListener('click',()=>{
  //   let videoState = video.classList.toggle('video-playing');
  //   if (videoState) {
  //     videoToggler.classList.remove('video-toggler-paused');
  //     videoToggler.classList.add('video-toggler-playing');
  //     video.play();
  //   } else {
  //     video.pause();
  //     videoToggler.classList.remove('video-toggler-playing');
  //     videoToggler.classList.add('video-toggler-paused');
  //   }
  // });


  /// Make Every Visit Count Animation

  const startAnimation = gsap.timeline({defaults:{ ease:"back(1)"}})
    .from(block.querySelectorAll(".waves-wrapper circle"), {
      scale:0,
      stagger:.2,
      transformOrigin:"center",
      duration:1.5
    })
    .from(block.querySelectorAll(".small-circle-wrapper path"), {
      opacity:0,
      duration:1,
    },"<1")
    .to(block.querySelector(".site-box"), {
      clipPath:"polygon(0% 0%, 0% 100%, 100% 100%, 100% 0%)",
      duration:1,
      ease:"power2.in"
    },"<")

    .to(block.querySelector(".numbers-move"), {
      clipPath:"polygon(0 0, 100% 0, 100% 100%, 0% 100%)",
      duration:.5,
      ease:"linear"
    })
    .from(block.querySelectorAll(".spotlight-base path"), {
      scale:0,
      transformOrigin:"center",
      stagger:.1
    },"<.5")
    .from(block.querySelectorAll(".spotlight .glow-wrapper"), {
      scale:0,
      transformOrigin:"bottom",
      stagger:.1,
      ease:"power2"
    },"<.2")

    .from(block.querySelectorAll(".clint"), {
      scale:0,
      x:-300,
      opacity:0,
      duration:1,
      transformOrigin:"center",
      ease:"back",
      stagger:.3
    },"<")

    .from(block.querySelectorAll(".wave"), {
      opacity:1,
      repeat:-1,
      duration:1,
      yoyo:true,
      ease:"steps(4)",
      stagger:.3
    })



  const count = gsap.timeline({defaults:{duration:3, ease:"linear", repeat:-1}})
    .to(block.querySelectorAll(".site-text-wrapper path"), {
      y:-95
    })
    .set(block.querySelector(".numbers-move g:last-child"),{xPercent:-100, x:-20})
    .fromTo(block.querySelector(".numbers-move"),{x:60,xPercent:0}, {x:70,xPercent:50})
    .to(block.querySelector(".spin-3"), {
      transformOrigin:"center",
      rotate:360,
      duration:5,
    },"<")
    .to(block.querySelector(".spin-2"), {
      transformOrigin:"50% 57%",
      rotate:360,
    },"<.2")
    .to(block.querySelector(".spin-1"), {
      transformOrigin:"center",
      rotate:360,
      duration:2,

    },"<.3")



  animations(block);
  imageLazyLoading(block);
};

export default heroBlock;

