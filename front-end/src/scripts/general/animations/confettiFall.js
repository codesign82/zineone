import {gsap} from 'gsap';
import {debounce} from "../../functions/debounce";
import {getElementsForAnimation} from '../../functions/getElementsForAnimation';
import {stringToHTML} from "../../functions/stringToHTML";

function R(min, max) {
  return min + Math.random() * (max - min)
}

const confetties = [
  stringToHTML(`<svg class="confetti-img" width="14" height="14" viewBox="0 0 14 14" fill="none"><circle cx="7" cy="7" r="7" fill="currentColor"/></svg>`)[0],
  stringToHTML(`<svg class="confetti-img" width="31" height="30" viewBox="0 0 30 30" fill="none"><line x1="2" y1="2" x2="28" y2="28" stroke-width="5" stroke="currentColor"/><line x1="28" y1="2" x2="2" y2="28" stroke-width="5" stroke="currentColor"/></svg>`)[0],
  stringToHTML(`<svg class="confetti-img" width="31" height="30" viewBox="0 0 30 30" fill="none"><line x1="28" y1="2" x2="2" y2="28" stroke-width="5" stroke="currentColor"/></svg>`)[0]]


export function confettiFall(block) {
  const canvases = getElementsForAnimation(block, '.confetti-fall');
  for (const canvas of canvases) {

    const total = block.dataset.confettiNumber || 30;

    const createConfetti = () => {
      const dimensions = canvas.getBoundingClientRect();
      canvas.innerHTML = '';
      for (let i = 0; i < total; i++) {
        const index = ~~(Math.random() * confetties.length);
        const confetti = confetties[index].cloneNode(true)
        canvas.appendChild(confetti);
        gsap.set(confetti, {
          x: R(0, dimensions.width),
          y: -20,
          scale: R(0.4, 0.9),
          color: `random(['#A0CD37','#32BBA9','#1174BB'])`
        });
        gsap.to(confetti, {
          duration: R(6, 15),
          y: dimensions.height + 20,
          ease: 'linear',
          repeat: -1,
          delay: -15
        });
        gsap.to(confetti, {
          duration: R(4, 8),
          x: Math.random()>0.5?'+=100':'-=100',
          repeat: -1,
          yoyo: true,
          ease: 'sine.inOut'
        });
        gsap.to(confetti, {
          duration: R(2, 8),
          rotation: index&&R(0, 360),
          repeat: -1,
          yoyo: true,
          ease: 'sine.inOut',
          delay: -5
        });
      }
    }

    createConfetti();
    window.addEventListener('resize', debounce(createConfetti, 200))
  }
}
