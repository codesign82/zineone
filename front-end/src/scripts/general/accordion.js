import gsap from "gsap";

export default function accordion(block) {
  const faqs = block.querySelectorAll('.faq');
  if (faqs.length === 0) return;
  faqs.forEach((faq) => {
    const faqHead = faq.querySelector('.faq-head');
    const faqBody = faq.querySelector('.faq-body');
    faqHead?.addEventListener('click', (e) => {
      if (!faqBody) {
        return;
      }
      const isOpened = faq?.classList.toggle('faq-active');
      if (!isOpened) {
        gsap.to(faqBody, {height: 0});
      } else {
        gsap.to(Array.from(faqs).map(otherFaq => {
          const otherFaqBody = otherFaq.querySelector('.faq-body');
          if (otherFaqBody && faq !== otherFaq) {
            otherFaq?.classList.remove('faq-active');
            gsap.set(otherFaq, {zIndex: 1});
          }
          return otherFaqBody;
        }), {height: 0});
        gsap.set(faq, {zIndex: 2});
        gsap.to(faqBody, {height: 'auto'});
      }
    });
  });
}
