<?php
get_header();
global $post;
//$thumbnail_id = get_post_thumbnail_id( $post->ID );
//$image_alt    = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );
$post_id           = get_the_ID();
$author_details    = get_author_details( $post_id );
$thumbnail_id      = get_post_thumbnail_id();
$alt               = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );
$short_description = get_field( 'short_description', $post_id );
$author_name       = $author_details['name'];
$author_image      = $author_details['img'];
$author_bio        = $author_details['bio'];
$twitter_link      = $author_details['twitter_link'];
$facebook_link     = $author_details['facebook_link'];
$linkedin_link     = $author_details['linkedin_link'];
?>
<?php if ( have_posts() ): the_post(); ?>
  <div class="single-post-wrapper">
    <div class="container">
      <h2 class="headline-1 post-title"><?php the_title(); ?></h2>
      <div class="post-information paragraph paragraph-16">
        <div class="author-name"><?= __( 'By', 'wrap' ) . ' ' . $author_name ?></div>
        <div class="post-info-separator">|</div>
        <div class="post-date"><?= get_the_date( 'j F, Y' ) ?></div>
        <!--        <div class="post-info-separator">|</div>-->
        <!--        <div class="post-comments">-->
        <? //= get_comments_number(); ?><!-- Comments</div>-->
      </div>
      <ul class="social-icons-wrapper reset-ul">
        <li class="social-icon">
          <a href="https://www.facebook.com/sharer?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>"
             target="_blank" rel="noopener noreferrer">
            <svg viewBox="0 0 11 20" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
              <path
                d="M2.80805 20V10.6154H0V7.23652H2.80805V4.3505C2.80805 2.08264 4.46677 0 8.28882 0C9.83631 0 10.9806 0.1311 10.9806 0.1311L10.8904 3.28642C10.8904 3.28642 9.72344 3.27638 8.44996 3.27638C7.07167 3.27638 6.85085 3.83768 6.85085 4.7693V7.23652H11L10.8195 10.6154H6.85085V20H2.80805Z"
                fill="#111222"/>
            </svg>
          </a></li>
        <li class="social-icon">
          <a href="http://twitter.com/intent/tweet?text=Currently reading <?php the_title(); ?>&amp;url=<?php the_permalink(); ?>"
             target="_blank" rel="noopener noreferrer">
            <svg viewBox="0 0 24 20" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
              <path
                d="M24 2.37575C23.1174 2.76628 22.157 3.04735 21.1676 3.15682C22.1948 2.53089 22.9639 1.54202 23.3308 0.375752C22.367 0.963885 21.3111 1.37629 20.2101 1.59469C19.7499 1.09003 19.1933 0.687999 18.575 0.413643C17.9567 0.139287 17.2899 -0.00151415 16.6163 1.22793e-05C13.8906 1.22793e-05 11.6986 2.26628 11.6986 5.04735C11.6986 5.43788 11.7447 5.82841 11.8197 6.20415C7.73849 5.98522 4.09855 3.98522 1.67864 0.923089C1.23771 1.69562 1.00665 2.57524 1.00949 3.47042C1.00949 5.2219 1.87766 6.76628 3.20154 7.67456C2.42136 7.64305 1.65944 7.42308 0.977767 7.03255V7.09468C0.977767 9.54734 2.66795 11.5799 4.92056 12.0473C4.49761 12.16 4.06251 12.2177 3.62553 12.2189C3.30537 12.2189 3.00252 12.1864 2.69679 12.142C3.31979 14.142 5.134 15.5947 7.29432 15.642C5.60413 17 3.48708 17.7988 1.18832 17.7988C0.775868 17.7988 0.395145 17.784 0 17.7367C2.18051 19.1716 4.7677 20 7.5539 20C16.599 20 21.5484 12.3136 21.5484 5.64202C21.5484 5.42309 21.5484 5.20415 21.534 4.98522C22.4915 4.26628 23.3308 3.37575 24 2.37575Z"
                fill="#111222"/>
            </svg>
          </a></li>
        <li class="social-icon">
          <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink() ?>&amp;title=<?php the_title(); ?>&amp;summary=&amp;source=<?php bloginfo( 'name' ); ?>"
             target="_new" rel="noopener noreferrer">
            <svg viewBox="0 0 21 22">
              <path fill="#111222"
                    d="M19.5.5c.797 0 1.5.703 1.5 1.547V20c0 .844-.703 1.5-1.5 1.5H1.453C.656 21.5 0 20.844 0 20V2.047C0 1.203.656.5 1.453.5zM6.328 8.516H3.234V18.5h3.094zm.234-3.188c0-.984-.796-1.828-1.78-1.828a1.83 1.83 0 0 0-1.829 1.828c0 .984.797 1.781 1.828 1.781a1.78 1.78 0 0 0 1.782-1.78zM18 13.016c0-2.672-.61-4.782-3.75-4.782-1.5 0-2.531.844-2.953 1.641h-.047v-1.36H8.297V18.5h3.094v-4.922c0-1.312.234-2.578 1.875-2.578 1.593 0 1.593 1.5 1.593 2.625V18.5H18z"/>
            </svg>
          </a></li>
        <li class="social-icon icon-copied">
          <a href="#">
            <svg width="20" height="20" viewBox="0 0 20 20">
              <path fill="#111222"
                    d="M11.78 15.415c1.954-1.955 1.954-5.2 0-7.194a6.339 6.339 0 0 0-1.643-1.095c-.235-.078-.548.04-.626.313l-.117.586c0 .196.117.391.313.47.39.156.743.43 1.055.743 1.447 1.407 1.447 3.753.04 5.16l-.04.04-3.088 3.089a3.658 3.658 0 0 1-5.2 0 3.658 3.658 0 0 1 0-5.2l2.932-2.933a.56.56 0 0 0 .156-.391c-.039-.196-.039-.391-.039-.587a.493.493 0 0 0-.821-.312l-.117.117-3.09 3.128c-1.994 1.955-1.994 5.2 0 7.155 1.956 1.994 5.201 1.994 7.156 0zM8.22 11.78c.51.47 1.057.821 1.643 1.095.235.078.548-.04.626-.313l.117-.586a.508.508 0 0 0-.313-.47c-.39-.156-.743-.43-1.055-.743-1.447-1.407-1.447-3.753-.04-5.16l.04-.04 3.089-3.089a3.658 3.658 0 0 1 5.2 0 3.658 3.658 0 0 1 0 5.2l-2.933 2.933a.56.56 0 0 0-.156.391c.039.196.039.391.039.587.04.39.508.586.821.312l.117-.117 3.09-3.128c1.994-1.955 1.994-5.2 0-7.155-1.956-1.995-5.201-1.995-7.156 0L8.221 4.585c-1.955 1.955-1.955 5.2 0 7.194z"/>
            </svg>
            <span class="copied-text"><?= __( 'Copied', 'wrap' ) ?></span>
          </a>
        </li>
      </ul>

      <picture class="single-blog-image">
        <img <?php acf_img( $thumbnail_id, '1280px', 'large' ) ?> alt="<?= $alt ?>">
      </picture>
      <?php if ( $short_description ) { ?>
        <div class="short-description paragraph"><?= $short_description ?></div>
      <?php } ?>
      <div class="blog-content">
        <div class="blog-content-wrapper"><?php the_content(); ?>
          <div class="post-information paragraph paragraph-16 hide-medium">
            <div class="author-name"><?= __( 'By', 'wrap' ) . ' ' . $author_name ?></div>
            <div class="post-info-separator">|</div>
            <div class="post-date"><?= get_the_date( 'j F, Y' ) ?></div>
            <!--        <div class="post-info-separator">|</div>-->
            <!--        <div class="post-comments">-->
            <? //= get_comments_number(); ?><!-- Comments</div>-->
          </div>
          <ul class="social-icons-wrapper reset-ul hide-medium">
            <li class="social-icon">
              <a href="https://www.facebook.com/sharer?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>"
                 target="_blank" rel="noopener noreferrer">
                <svg viewBox="0 0 11 20" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M2.80805 20V10.6154H0V7.23652H2.80805V4.3505C2.80805 2.08264 4.46677 0 8.28882 0C9.83631 0 10.9806 0.1311 10.9806 0.1311L10.8904 3.28642C10.8904 3.28642 9.72344 3.27638 8.44996 3.27638C7.07167 3.27638 6.85085 3.83768 6.85085 4.7693V7.23652H11L10.8195 10.6154H6.85085V20H2.80805Z"
                    fill="#111222"/>
                </svg>
              </a></li>
            <li class="social-icon">
              <a href="http://twitter.com/intent/tweet?text=Currently reading <?php the_title(); ?>&amp;url=<?php the_permalink(); ?>"
                 target="_blank" rel="noopener noreferrer">
                <svg viewBox="0 0 24 20" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M24 2.37575C23.1174 2.76628 22.157 3.04735 21.1676 3.15682C22.1948 2.53089 22.9639 1.54202 23.3308 0.375752C22.367 0.963885 21.3111 1.37629 20.2101 1.59469C19.7499 1.09003 19.1933 0.687999 18.575 0.413643C17.9567 0.139287 17.2899 -0.00151415 16.6163 1.22793e-05C13.8906 1.22793e-05 11.6986 2.26628 11.6986 5.04735C11.6986 5.43788 11.7447 5.82841 11.8197 6.20415C7.73849 5.98522 4.09855 3.98522 1.67864 0.923089C1.23771 1.69562 1.00665 2.57524 1.00949 3.47042C1.00949 5.2219 1.87766 6.76628 3.20154 7.67456C2.42136 7.64305 1.65944 7.42308 0.977767 7.03255V7.09468C0.977767 9.54734 2.66795 11.5799 4.92056 12.0473C4.49761 12.16 4.06251 12.2177 3.62553 12.2189C3.30537 12.2189 3.00252 12.1864 2.69679 12.142C3.31979 14.142 5.134 15.5947 7.29432 15.642C5.60413 17 3.48708 17.7988 1.18832 17.7988C0.775868 17.7988 0.395145 17.784 0 17.7367C2.18051 19.1716 4.7677 20 7.5539 20C16.599 20 21.5484 12.3136 21.5484 5.64202C21.5484 5.42309 21.5484 5.20415 21.534 4.98522C22.4915 4.26628 23.3308 3.37575 24 2.37575Z"
                    fill="#111222"/>
                </svg>
              </a></li>
            <li class="social-icon">
              <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink() ?>&amp;title=<?php the_title(); ?>&amp;summary=&amp;source=<?php bloginfo( 'name' ); ?>"
                 target="_new" rel="noopener noreferrer">
                <svg viewBox="0 0 21 22">
                  <path fill="#111222"
                        d="M19.5.5c.797 0 1.5.703 1.5 1.547V20c0 .844-.703 1.5-1.5 1.5H1.453C.656 21.5 0 20.844 0 20V2.047C0 1.203.656.5 1.453.5zM6.328 8.516H3.234V18.5h3.094zm.234-3.188c0-.984-.796-1.828-1.78-1.828a1.83 1.83 0 0 0-1.829 1.828c0 .984.797 1.781 1.828 1.781a1.78 1.78 0 0 0 1.782-1.78zM18 13.016c0-2.672-.61-4.782-3.75-4.782-1.5 0-2.531.844-2.953 1.641h-.047v-1.36H8.297V18.5h3.094v-4.922c0-1.312.234-2.578 1.875-2.578 1.593 0 1.593 1.5 1.593 2.625V18.5H18z"/>
                </svg>
              </a></li>
            <li class="social-icon icon-copied">
              <a class="icon" href="#">
                <svg width="20" height="20" viewBox="0 0 20 20">
                  <path fill="#111222"
                        d="M11.78 15.415c1.954-1.955 1.954-5.2 0-7.194a6.339 6.339 0 0 0-1.643-1.095c-.235-.078-.548.04-.626.313l-.117.586c0 .196.117.391.313.47.39.156.743.43 1.055.743 1.447 1.407 1.447 3.753.04 5.16l-.04.04-3.088 3.089a3.658 3.658 0 0 1-5.2 0 3.658 3.658 0 0 1 0-5.2l2.932-2.933a.56.56 0 0 0 .156-.391c-.039-.196-.039-.391-.039-.587a.493.493 0 0 0-.821-.312l-.117.117-3.09 3.128c-1.994 1.955-1.994 5.2 0 7.155 1.956 1.994 5.201 1.994 7.156 0zM8.22 11.78c.51.47 1.057.821 1.643 1.095.235.078.548-.04.626-.313l.117-.586a.508.508 0 0 0-.313-.47c-.39-.156-.743-.43-1.055-.743-1.447-1.407-1.447-3.753-.04-5.16l.04-.04 3.089-3.089a3.658 3.658 0 0 1 5.2 0 3.658 3.658 0 0 1 0 5.2l-2.933 2.933a.56.56 0 0 0-.156.391c.039.196.039.391.039.587.04.39.508.586.821.312l.117-.117 3.09-3.128c1.994-1.955 1.994-5.2 0-7.155-1.956-1.995-5.201-1.995-7.156 0L8.221 4.585c-1.955 1.955-1.955 5.2 0 7.194z"/>
                </svg>
                <span class="copied-text"><?= __( 'copied', 'rent-reporters-theme' ) ?></span>
              </a>
            </li>
          </ul>
        </div>
        <div class="author-information">
          <picture class="author-image aspect-ratio">
            <img src="<?= $author_image['url'] ?>" alt="<?= $author_image['alt'] ?>">
          </picture>
          <?php if ( $author_bio ) { ?>
            <div class="author-bio paragraph paragraph-16"><?= $author_bio ?></div>
          <?php } ?>
          <?php if ( $facebook_link || $twitter_link || $linkedin_link ) { ?>
            <ul class="social-icons-wrapper reset-ul">
              <?php if ( $facebook_link ) { ?>
                <li class="social-icon">
                  <a href="<?= $facebook_link ?>"
                     target="_blank" rel="noopener noreferrer">
                    <svg viewBox="0 0 11 20" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M2.80805 20V10.6154H0V7.23652H2.80805V4.3505C2.80805 2.08264 4.46677 0 8.28882 0C9.83631 0 10.9806 0.1311 10.9806 0.1311L10.8904 3.28642C10.8904 3.28642 9.72344 3.27638 8.44996 3.27638C7.07167 3.27638 6.85085 3.83768 6.85085 4.7693V7.23652H11L10.8195 10.6154H6.85085V20H2.80805Z"
                        fill="#111222"/>
                    </svg>
                  </a>
                </li>
              <?php } ?>
              <?php if ( $twitter_link ) { ?>
                <li class="social-icon">
                  <a href="<?= $twitter_link ?>"
                     target="_blank" rel="noopener noreferrer">
                    <svg viewBox="0 0 24 20" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M24 2.37575C23.1174 2.76628 22.157 3.04735 21.1676 3.15682C22.1948 2.53089 22.9639 1.54202 23.3308 0.375752C22.367 0.963885 21.3111 1.37629 20.2101 1.59469C19.7499 1.09003 19.1933 0.687999 18.575 0.413643C17.9567 0.139287 17.2899 -0.00151415 16.6163 1.22793e-05C13.8906 1.22793e-05 11.6986 2.26628 11.6986 5.04735C11.6986 5.43788 11.7447 5.82841 11.8197 6.20415C7.73849 5.98522 4.09855 3.98522 1.67864 0.923089C1.23771 1.69562 1.00665 2.57524 1.00949 3.47042C1.00949 5.2219 1.87766 6.76628 3.20154 7.67456C2.42136 7.64305 1.65944 7.42308 0.977767 7.03255V7.09468C0.977767 9.54734 2.66795 11.5799 4.92056 12.0473C4.49761 12.16 4.06251 12.2177 3.62553 12.2189C3.30537 12.2189 3.00252 12.1864 2.69679 12.142C3.31979 14.142 5.134 15.5947 7.29432 15.642C5.60413 17 3.48708 17.7988 1.18832 17.7988C0.775868 17.7988 0.395145 17.784 0 17.7367C2.18051 19.1716 4.7677 20 7.5539 20C16.599 20 21.5484 12.3136 21.5484 5.64202C21.5484 5.42309 21.5484 5.20415 21.534 4.98522C22.4915 4.26628 23.3308 3.37575 24 2.37575Z"
                        fill="#111222"/>
                    </svg>
                  </a>
                </li>
              <?php } ?>
              <?php if ( $linkedin_link ) { ?>
                <li class="social-icon">
                  <a href="<?= $linkedin_link ?>"
                     target="_new" rel="noopener noreferrer">
                    <svg viewBox="0 0 21 22">
                      <path fill="#111222"
                            d="M19.5.5c.797 0 1.5.703 1.5 1.547V20c0 .844-.703 1.5-1.5 1.5H1.453C.656 21.5 0 20.844 0 20V2.047C0 1.203.656.5 1.453.5zM6.328 8.516H3.234V18.5h3.094zm.234-3.188c0-.984-.796-1.828-1.78-1.828a1.83 1.83 0 0 0-1.829 1.828c0 .984.797 1.781 1.828 1.781a1.78 1.78 0 0 0 1.782-1.78zM18 13.016c0-2.672-.61-4.782-3.75-4.782-1.5 0-2.531.844-2.953 1.641h-.047v-1.36H8.297V18.5h3.094v-4.922c0-1.312.234-2.578 1.875-2.578 1.593 0 1.593 1.5 1.593 2.625V18.5H18z"/>
                    </svg>
                  </a>
                </li>
              <?php } ?>
            </ul>
          <?php } ?>
        </div>
      </div>
      <div class="other-blogs">
        <?php
        $related_query = new WP_Query( array(
          'post_type'      => 'post',
          'category__in'   => wp_get_post_categories( get_the_ID() ),
          'post__not_in'   => array( get_the_ID() ),
          'posts_per_page' => 3,
          'orderby'        => 'date',
        ) );
        if ( $related_query->have_posts() ) : ?>
          <div class="related-blogs">
            <h2 class="headline-2 related-blogs-title"><?= __( 'Other blogs', 'wrap' ) ?></h2>
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
              <?php while ( $related_query->have_posts() ) {
                $related_query->the_post();
                get_template_part( "template-parts/post-card" );
                ?>
              <?php } ?>
            </div>
          </div>
        <?php endif;
        wp_reset_postdata(); ?>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php
get_footer();
