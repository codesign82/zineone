<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'featured_resources_block';
$className = 'featured_resources_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/featured_resources_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$section_title = get_field('section_title');
$featured_resources = get_field('featured_resources');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="feature">
    <?php if ($section_title) { ?>
      <h2 class="headline-2 iv-st-from-bottom">
        <?= $section_title ?>
      </h2>
    <?php } ?>

    <?php
    if ($featured_resources): ?>
      <div class="feature-content">
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
          <?php foreach ($featured_resources as $post):
            setup_postdata($post);
            get_template_part('template-parts/post-card','',array('post_id'=>$post->ID));
          endforeach;
          wp_reset_postdata(); ?>
        </div>
      </div>
    <?php endif; ?>

  </div>
</div>
</section>


<!-- endregion ZineOne's Block -->
