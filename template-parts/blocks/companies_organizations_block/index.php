<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'companies_organizations_block';
$className = 'companies_organizations_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/companies_organizations_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
  <div class="container">
    <div class="companies-organizations">
      <h2 class="headline-2">Great companies and organizations invest in our
        company</h2>
      <div class="companies-box box-zineOne">
        <picture class="companies-img">
          <img src="<?= get_template_directory_uri() . '/front-end/src/images/norwest.png' ?>" alt="">
        </picture>
        <picture class="companies-img">
          <img src="<?= get_template_directory_uri() . '/front-end/src/images/flourish.png' ?>" alt="">
        </picture>
        <picture class="companies-img">
          <img src="<?= get_template_directory_uri() . '/front-end/src/images/kapital-partner.png' ?>" alt="">
        </picture>
        <picture class="companies-img">
          <img src="<?= get_template_directory_uri() . '/front-end/src/images/touchstone.png' ?>" alt="">
        </picture>
      </div>
    </div>
  </div>
</section>


<!-- endregion ZineOne's Block -->
