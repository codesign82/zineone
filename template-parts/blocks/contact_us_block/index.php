<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'contact_us_block';
$className = 'contact_us_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/contact_us_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
  <div class="container">
    <div class="contact_us_head">
      <h2 class="headline-1">Contact Us</h2>
      <p class="paragraph">Have a question? Need information?</p>
      <form class="contact-us-form">
        <input class="contact-us-form-input box-shadow" type="text"
               name="fullname"
               placeholder="Full Name">
        <input class="contact-us-form-input box-shadow" type="email"
               name="Work Email"
               placeholder="Work Email">
        <input class="contact-us-form-input box-shadow" type="text"
               name="Compant Name"
               placeholder="Compant Name">
        <input class="contact-us-form-input box-shadow" type="text"
               name="Phone Number"
               placeholder="Phone Number">
        <div class="selector-item">
          <select
            class="contact-us-form-input box-shadow selector" name="Reason">
            <option value="volvo">Reason</option>
          </select></div>
        <textarea id="message"
                  class="contact-us-form-input box-shadow input-message"
                  name="Message"
                  placeholder="Message"></textarea>
      </form>
      <p class="paragraph last-text">We care about your privacy and will not
        share your
        personal information.</p>
      <a href="#" class="btn">Submit</a>
    </div>
  </div>
</section>


<!-- endregion ZineOne's Block -->
