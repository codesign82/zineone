<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'page_title_card_info_block';
$className = 'page_title_card_info_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/page_title_card_info_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$small_title = get_field('small_title');
$title = get_field('title');
$text_content = get_field('text_content');
$content_max_width = get_field('content_max_width');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <?php if ($content_max_width) { ?>
    <style>
      <?='#'.$id?>
      .content-wrapper {
        max-width: <?=$content_max_width.'%'?:'80'.'%'?>;
      }
    </style>
  <?php } ?>
  <div class="page_card box-zineOne">
    <div class="content-wrapper">
      <?php if ($small_title) { ?>
        <h6 class="head-text iv-st-from-bottom"><?= $small_title ?></h6>
      <?php } ?>
      <?php if ($title) { ?>
        <h2 class="headline-3 iv-st-from-bottom"><?= $title ?></h2>
      <?php } ?>
      <?php if ($text_content) { ?>
        <div class="iv-st-from-bottom">
          <?= $text_content; ?>
        </div>
      <?php } ?>
      <?php if (have_rows('logos')) { ?>
        <div class="page_title_images">
          <?php while (have_rows('logos')) {
            the_row();
            $logo = get_sub_field('logo');
            $url = get_sub_field('url');
            ?>
            <picture class="page_title_img iv-st-from-bottom">
              <a href="<?= $url ?>" target="_blank">
                <img src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>">
              </a>
            </picture>
          <?php } ?>
        </div>
      <?php } ?>
    </div>
  </div>

</div>
</section>


<!-- endregion ZineOne's Block -->
