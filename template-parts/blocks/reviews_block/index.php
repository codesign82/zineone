<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'reviews_block';
$className = 'reviews_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/reviews_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
  <div class="container">
    <div class="reviews-head">
      <picture class="reviews-img">
        <img src="<?= get_template_directory_uri() . '/front-end/src/images/review-img.png' ?>" alt="">
      </picture>
      <h2 class="headline-3">“We’re excited to welcome ZineOne to Microsoft AppSource…”</h2>
      <p class="paragraph">-Kirsten Edmondson Wolfe, Sr. Director, AppSource Product Marketing, Microsoft Corp.</p>
    </div>

  </div>
</section>


<!-- endregion ZineOne's Block -->
