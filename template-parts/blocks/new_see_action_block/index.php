<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'new_see_action_block';
$className = 'new_see_action_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/new_see_action_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$video_type = get_field('video_type');
$video_file = get_field('video_file');
$video_url = get_field('video_url');

?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
  <div class="block-title headline-3">See it in Action</div>
  <div class="aspect-ratio video-wrapper">
    <video
      src="<?= get_template_directory_uri() ?>/front-end/src/images/video-block.mp4"
      type="video/mp4" class=" video">

    </video>
    <div class="svg-wrapper  ">
      <svg width="118" height="118" viewBox="0 0 118 118" fill="none"
           xmlns="http://www.w3.org/2000/svg">
        <path opacity="0.2"
              d="M0 59C0 26.432 26.432 0 59 0C91.568 0 118 26.432 118 59C118 91.568 91.568 118 59 118C26.432 118 0 91.568 0 59Z"
              fill="url(#paint0_linear_59_2811)"/>
        <path opacity="0.4"
              d="M16.3887 59C16.3887 35.4786 35.4784 16.3889 58.9998 16.3889C82.5211 16.3889 101.611 35.4786 101.611 59C101.611 82.5213 82.5211 101.611 58.9998 101.611C35.4784 101.611 16.3887 82.5213 16.3887 59Z"
              fill="url(#paint1_linear_59_2811)"/>
        <path
          d="M58.9999 31.6855C43.9221 31.6855 31.6851 43.9225 31.6851 59.0003C31.6851 74.0781 43.9221 86.3151 58.9999 86.3151C74.0777 86.3151 86.3147 74.0781 86.3147 59.0003C86.3147 43.9225 74.0777 31.6855 58.9999 31.6855ZM53.5369 68.5605V49.4401C53.5369 48.3202 54.8207 47.6647 55.7221 48.3475L68.4781 57.9077C69.2156 58.454 69.2156 59.5466 68.4781 60.0929L55.7221 69.6531C54.8207 70.3359 53.5369 69.6804 53.5369 68.5605Z"
          fill="url(#paint2_linear_59_2811)"/>
        <defs>
          <linearGradient id="paint0_linear_59_2811" x1="-5.67338e-06"
                          y1="118" x2="117.649" y2="-0.348587"
                          gradientUnits="userSpaceOnUse">
            <stop stop-color="#FB762B"/>
            <stop offset="1" stop-color="#F2512D"/>
          </linearGradient>
          <linearGradient id="paint1_linear_59_2811" x1="16.3887"
                          y1="101.611" x2="101.358" y2="16.1371"
                          gradientUnits="userSpaceOnUse">
            <stop stop-color="#FB762B"/>
            <stop offset="1" stop-color="#F2512D"/>
          </linearGradient>
          <linearGradient id="paint2_linear_59_2811" x1="31.6851"
                          y1="86.3151" x2="86.1523" y2="31.5241"
                          gradientUnits="userSpaceOnUse">
            <stop stop-color="#FB762B"/>
            <stop offset="1" stop-color="#F2512D"/>
          </linearGradient>
        </defs>
      </svg>
    </div>
  </div>

</div>
</section>


<!-- endregion ZineOne's Block -->
