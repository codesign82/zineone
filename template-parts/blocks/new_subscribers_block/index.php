<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'new_subscribers_block';
$className = 'new_subscribers_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/new_subscribers_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
  <div class="content">
    <div class="row">
      <?php if (have_rows('card')) {
        while (have_rows('card')) {
          the_row();
          $card_image = get_sub_field("card_image")
          ?>
          <div class="col-md-3">
            <picture class="img-wrapper">
              <img
                src="<?= $card_image["url"]?>"
                alt="<?= $card_image["alt"] ?>">
            </picture>
          </div>
        <?php }
      } ?>


    </div>
  </div>
</div>
</section>


<!-- endregion ZineOne's Block -->
