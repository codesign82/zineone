<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'global_offices_block';
$className = 'global_offices_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/global_offices_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
  <div class="container">
    <h2 class="headline-2 text1">Global Offices</h2>
    <div class="about-global-offices">
      <div class="row row-cols-1 row-cols-md-2 row-cols-lg-2">

        <div class="col">
          <div class="global-offices-head">
            <p class="paragraph head4">Global Headquarters</p>
            <h2 class="headline-3">Milpitas, California</h2>
            <p class="paragraph head1">1900 McCarthy Blvd, Suite 420<br>
              Milpitas, CA 95035 </p>
            <p class="paragraph head2">Our Silicon Valley HQ is home for our
              core teams,
              from product and engineering to sales and customer success.</p>
          </div>
        </div>

        <div class="col">
          <div class="global-offices-head">
            <p class="paragraph head4">India Headquarters</p>
            <h2 class="headline-3">Mumbai, India</h2>
            <p class="paragraph head3">91Springboard Business Hub, 74/II "C"
              Cross Road
              Opp Gate #2, Andheri East Mumbai, Mumbai City, MH 400093</p>
            <p class="paragraph head2">Our engineering and data science teams
              are set in
              one of India’s most dynamic business hubs.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="globally-offices-text">
      <div class="leader-office">
        <p class="paragraph">Founded in</p>
        <h3 class="headline-2 text2">2015</h3>
      </div>
      <div class="leader-office">
        <p class="paragraph">Globally</p>
        <h3 class="headline-2 text3">70+ People</h3>
      </div>
      <div class="leader-office">
        <p class="paragraph">leadership positions are occupied by women</p>
        <h3 class="headline-2 text4">36.4%</h3>
      </div>
    </div>
  </div>
</section>


<!-- endregion ZineOne's Block -->
