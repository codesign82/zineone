<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'about_the_platform_block';
$className = 'about_the_platform_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/about_the_platform_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
  <div class="container">
    <div class="about-platform">
      <div class="about-platform-text">
        <h2 class="headline-2">About the platform</h2>
        <p class="paragraph">ZineOne enables large enterprises in the retail,
          restaurant, hospitality and banking industries to understand and
          respond
          in real-time to individual customer activity, solving the challenge
          of
          personalized engagement at scale.<br>

          ZineOne’s Real-Time Marketing platform combines and processes an
          unprecedented amount of past and present customer data points, then
          leverages AI to react to customer triggers in real-time and on any
          channel, engaging each customer at the right time, in the right
          place,
          with the right message.
        </p>
      </div>
      <div class="about-platform-numbers">
        <div class="about-platform-numbers-text">
          <p class="paragraph">More Than</p>
          <h5 class="headline-1 text1">5</h5>
          <p class="paragraph text1">profiles</p>
        </div>
        <div class="about-platform-numbers-text">
          <p class="paragraph">Processing over</p>
          <h5 class="headline-1 text2">8B</h5>
          <p class="paragraph text2">interactions each year</p>
        </div>
        <div class="about-platform-numbers-text">
          <p class="paragraph">Analyzing</p>
          <h5 class="headline-1 text3">300GB</h5>
          <p class="paragraph text3">of data every hour</p>
        </div>
      </div>
    </div>
  </div>
</section>


<!-- endregion ZineOne's Block -->
