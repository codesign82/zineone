<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'new_deploy_block';
$className = 'new_deploy_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/new_deploy_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$image = get_field('image');

?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="content">
    <?php if ($image) { ?>
      <div class="left-content large">
        <picture class="img-wrapper">
          <img
            src="<?= $image["url"] ?>"
            alt="<?= $image["alt"] ?>">
        </picture>
      </div>
    <?php } ?>

    <div class="right-content small">
      <div class="heading headline-2">
        <?= $title ?>
      </div>
      <div class="description paragraph">
        <?= $description ?>
      </div>
    </div>
  </div>
</div>

</section>


<!-- endregion ZineOne's Block -->
