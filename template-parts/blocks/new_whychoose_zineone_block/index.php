<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'new_whychoose_zineone_block';
$className = 'new_whychoose_zineone_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/new_whychoose_zineone_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');

?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
  <?php if ($title) { ?>
    <h2 class="headline-2"><?= $title; ?></h2>
  <?php } ?>
  <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4">
    <?php if (have_rows('cards')) {
      while (have_rows('cards')) {
        the_row();
        $svg_color = get_sub_field('svg_color');
        $card_title = get_sub_field('card_title');
        $description = get_sub_field('description'); ?>
        <div class="col">
          <div class="why-choose_group box-shadow">

            <div class="why-choose_img svg_border-color-1">
              <svg width="68" height="68" viewBox="0 0 68 68" fill="none"
                   xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M34 0.666626C15.6 0.666626 0.666626 15.6 0.666626 34C0.666626 52.4 15.6 67.3333 34 67.3333C52.4 67.3333 67.3333 52.4 67.3333 34C67.3333 15.6 52.4 0.666626 34 0.666626ZM17.5666 54.9333C19 51.9333 27.7333 49 34 49C40.2666 49 49.0333 51.9333 50.4333 54.9333C45.9 58.5333 40.2 60.6666 34 60.6666C27.8 60.6666 22.1 58.5333 17.5666 54.9333ZM55.2 50.1C50.4333 44.3 38.8666 42.3333 34 42.3333C29.1333 42.3333 17.5666 44.3 12.8 50.1C9.39996 45.6333 7.33329 40.0666 7.33329 34C7.33329 19.3 19.3 7.33329 34 7.33329C48.7 7.33329 60.6666 19.3 60.6666 34C60.6666 40.0666 58.6 45.6333 55.2 50.1ZM34 14C27.5333 14 22.3333 19.2 22.3333 25.6666C22.3333 32.1333 27.5333 37.3333 34 37.3333C40.4666 37.3333 45.6666 32.1333 45.6666 25.6666C45.6666 19.2 40.4666 14 34 14ZM34 30.6666C31.2333 30.6666 29 28.4333 29 25.6666C29 22.9 31.2333 20.6666 34 20.6666C36.7666 20.6666 39 22.9 39 25.6666C39 28.4333 36.7666 30.6666 34 30.6666Z"
                  fill="#A0CD37"/>
              </svg>
            </div>
            <?php if ($card_title) { ?>
              <h2 class="headline-3"><?= $card_title; ?></h2>
            <?php } ?>
            <?php if ($description) { ?>
              <div class="paragraph"><?= $description; ?></div>
            <?php } ?>
          </div>
        </div>
      <?php }
    } ?>
  </div>
</div>
</div>
</section>


<!-- endregion ZineOne's Block -->
