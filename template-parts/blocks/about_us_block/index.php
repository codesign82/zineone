<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'about_us_block';
$className = 'about_us_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/about_us_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
  <div class="container">
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4">
      <div class="col ">
        <div class="about-us-group box-zineOne">
          <div class="bottom-border"></div>
          <h2 class="headline-3">We are one team</h2>
          <p class="paragraph">We value authentic relationships.<br>
            We commit to open lines of communication to
            drive us forward.<br>
            We push together toward our shared goals,
            regardless of role or title
          </p>
        </div>
      </div>
      <div class="col ">
        <div class="about-us-group box-zineOne">
          <div class="bottom-border team1"></div>
          <h2 class="headline-3">We dream big & we deliver</h2>
          <p class="paragraph">We innovate beyond any comfort zone<br>
            We tackle hard problems <br>
            We do what needs to be done. No shortcuts
          </p>

        </div>
      </div>
      <div class="col ">
        <div class="about-us-group box-zineOne">
          <div class="bottom-border team2"></div>
          <h2 class="headline-3">We cultivate excellence</h2>
          <p class="paragraph">We work with discipline<br>
            We tenaciously hone our skills<br>
            Our grit shows up in our customer
            relationships, our products, and our results
          </p>
        </div>
      </div>
      <div class="col ">
        <div class="about-us-group box-zineOne">
          <div class="bottom-border team3"></div>
          <h2 class="headline-3">We act with integrity</h2>
          <p class="paragraph">We build on trust<br>

            Our work is grounded in honesty and mutual
            respect<br>
            No compromises on ethics
          </p>

        </div>

      </div>
    </div>
  </div>
</section>


<!-- endregion ZineOne's Block -->
