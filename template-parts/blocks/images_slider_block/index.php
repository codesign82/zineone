<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'images_slider_block';
$className = 'images_slider_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/images_slider_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container iv-st-from-bottom">
  <?php
  $images = get_field('image_slider');
  if ($images): ?>
    <div class="swiper-container">
      <div class="swiper-wrapper">
        <?php foreach ($images as $image): ?>
          <div class="swiper-slide">
            <picture class="slider-img">
              <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>"/>
              <p class="caption"><?php echo esc_html($image['caption']); ?></p>
            </picture>
          </div>
        <?php endforeach; ?>
      </div>
      <!-- Add Arrows -->
      <div class="swiper-button swiper-button-next">
        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
             xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd"
                d="M0 7C0 6.73483 0.105345 6.48052 0.292861 6.29302C0.480376 6.10552 0.734701 6.00018 0.999888 6.00018H12.5846L8.29107 1.70896C8.10332 1.52122 7.99784 1.26659 7.99784 1.00108C7.99784 0.735579 8.10332 0.48095 8.29107 0.293211C8.47883 0.105471 8.73347 0 8.99899 0C9.26452 0 9.51916 0.105471 9.70692 0.293211L15.7062 6.29213C15.7994 6.385 15.8732 6.49533 15.9236 6.6168C15.9741 6.73827 16 6.86849 16 7C16 7.13151 15.9741 7.26173 15.9236 7.3832C15.8732 7.50467 15.7994 7.615 15.7062 7.70787L9.70692 13.7068C9.51916 13.8945 9.26452 14 8.99899 14C8.73347 14 8.47883 13.8945 8.29107 13.7068C8.10332 13.5191 7.99784 13.2644 7.99784 12.9989C7.99784 12.7334 8.10332 12.4788 8.29107 12.291L12.5846 7.99982H0.999888C0.734701 7.99982 0.480376 7.89448 0.292861 7.70698C0.105345 7.51948 0 7.26517 0 7Z"
                fill="#1174BB"/>
        </svg>
      </div>
      <div class="swiper-button swiper-button-prev">
        <svg width="16" height="14" viewBox="0 0 16 14" fill="none"
             xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd"
                d="M16 7C16 6.73483 15.8947 6.48052 15.7071 6.29302C15.5196 6.10552 15.2653 6.00018 15.0001 6.00018H3.41541L7.70893 1.70896C7.89668 1.52122 8.00216 1.26659 8.00216 1.00108C8.00216 0.735579 7.89668 0.48095 7.70893 0.293211C7.52117 0.105471 7.26653 0 7.00101 0C6.73548 0 6.48084 0.105471 6.29308 0.293211L0.293755 6.29213C0.200638 6.385 0.12676 6.49533 0.0763531 6.6168C0.0259457 6.73827 0 6.86849 0 7C0 7.13151 0.0259457 7.26173 0.0763531 7.3832C0.12676 7.50467 0.200638 7.615 0.293755 7.70787L6.29308 13.7068C6.48084 13.8945 6.73548 14 7.00101 14C7.26653 14 7.52117 13.8945 7.70893 13.7068C7.89668 13.5191 8.00216 13.2644 8.00216 12.9989C8.00216 12.7334 7.89668 12.4788 7.70893 12.291L3.41541 7.99982H15.0001C15.2653 7.99982 15.5196 7.89448 15.7071 7.70698C15.8947 7.51948 16 7.26517 16 7Z"
                fill="#1174BB"/>
        </svg>
      </div>
    </div>
  <?php endif; ?>

</div>
</section>


<!-- endregion ZineOne's Block -->
