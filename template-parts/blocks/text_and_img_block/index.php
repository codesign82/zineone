<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'text_and_img_block';
$className = 'text_and_img_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_and_img_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
$cta_button = get_field('cta_button');
$image = get_field('image');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="zineOne-team">
    <div class="zineOne-team-text">
      <?php if ($title) { ?>
        <h2 class="headline-2 iv-st-from-left"><?= $title ?></h2>
      <?php } ?>
      <?php if ($description) { ?>
        <div class="paragraph iv-st-from-left">
          <?= $description ?>
        </div>
      <?php } ?>
      <?php if ($cta_button) { ?>
        <a href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>" class="btn iv-st-from-left">
          <?= $cta_button['title'] ?>
        </a>
      <?php } ?>
    </div>
    <?php if ($image) { ?>
      <picture class="team-img iv-st-from-right">
        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
      </picture>
    <?php } ?>
  </div>
</div>
</section>


<!-- endregion ZineOne's Block -->
