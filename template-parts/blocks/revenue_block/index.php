<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'revenue_block';
$className = 'revenue_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/revenue_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
  <div class="container">
    <div class="revenue">
      <div class="revenue-shapes">
        <div class="square1">
          <img src="<?= get_template_directory_uri() . '/front-end/src/images/Group%202535.svg' ?>" alt="">
        </div>
        <div class="circle">
          <h2>
            <span>Revenue</span>
            +20%
          </h2>
        </div>
        <div class="square2">
          <img src="<?= get_template_directory_uri() . '/front-end/src/images/Group%202539.svg' ?>" alt="">
        </div>
      </div>
      <div class="revenue-header">
        <div class="revenue-text">
          <div class="revenue-head">
            <h6 class="headline-2">5</h6>
            <p class="paragraph">Clicks</p>
          </div>
          <div class="revenue-head">
            <h6 class="headline-2">10</h6>
            <p class="paragraph">milliseconds</p>
          </div>
          <div class="revenue-head">
            <h6 class="headline-2">+20%</h6>
            <p class="paragraph">Revenue</p>
          </div>
        </div>
        <div class="revenue_details">
          <p class="paragraph">Harnessing the power of Machine Learning on
            behavior that is happening in the moment to predict what your
            visitor will do next.</p>
          <a class="learn-more" href="#">Learn More
            <svg width="16" height="14" viewBox="0 0 16 14" fill="none">
              <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M0 7C0 6.73483 0.105345 6.48052 0.292861 6.29302C0.480376 6.10552 0.734701 6.00018 0.999888 6.00018H12.5846L8.29107 1.70896C8.10332 1.52122 7.99784 1.26659 7.99784 1.00108C7.99784 0.735579 8.10332 0.48095 8.29107 0.293211C8.47883 0.105471 8.73347 0 8.99899 0C9.26452 0 9.51916 0.105471 9.70692 0.293211L15.7062 6.29213C15.7994 6.385 15.8732 6.49533 15.9236 6.6168C15.9741 6.73827 16 6.86849 16 7C16 7.13151 15.9741 7.26173 15.9236 7.3832C15.8732 7.50467 15.7994 7.615 15.7062 7.70787L9.70692 13.7068C9.51916 13.8945 9.26452 14 8.99899 14C8.73347 14 8.47883 13.8945 8.29107 13.7068C8.10332 13.5191 7.99784 13.2644 7.99784 12.9989C7.99784 12.7334 8.10332 12.4788 8.29107 12.291L12.5846 7.99982H0.999888C0.734701 7.99982 0.480376 7.89448 0.292861 7.70698C0.105345 7.51948 0 7.26517 0 7Z"
                    fill="white"/>
            </svg>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>


<!-- endregion ZineOne's Block -->
