<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'text_and_description_block';
$className = 'text_and_description_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/text_and_description_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title                = get_field( 'title' );
$is_has_bottom_border = get_field( 'is_has_bottom_border' );
$description          = get_field( 'description' );
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="content">
    <?php if ( $title ) { ?>
    <?php if ($is_has_bottom_border): ?>
      <h3 class="title headline-3 bottom-border centered iv-st-from-bottom"><?= $title ?></h3>
    <?php else: ?>
      <h2 class="title headline-2 iv-st-from-bottom"><?= $title ?></h2>
    <?php endif; ?>
  <?php } ?>
  <?php if ( $description ) { ?>
    <div class="paragraph iv-st-from-bottom"><?= $description ?></div>
  <?php } ?>
</div>
</div>
</section>


<!-- endregion ZineOne's Block -->
