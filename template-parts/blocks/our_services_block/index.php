<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'our_services_block';
$className = 'our_services_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/our_services_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <?php if (have_rows('services_cards')) { ?>
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
      <?php while (have_rows('services_cards')) {
        the_row();
        $image = get_sub_field('image');
        $title = get_sub_field('title');
        $url = get_sub_field('url');
        $text = get_sub_field('text');
        ?>
        <div class="col iv-st-from-bottom">
          <div class="our_services_group">
            <?php if ($image) { ?>
              <div class="our_services_img_wrapper">
                <picture class="our_services_img">
                  <?php if ($url){ ?>
                  <a href="<?= $url ?>" target="_blank"><?php } ?>
                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                    <?php if ($url){ ?> </a><?php } ?>
                </picture>
              </div>
            <?php } ?>
            <?php if ($title) { ?>
              <h2 class="headline-3">
                <?= $title ?>
              </h2>
            <?php } ?>
            <?php if ($text) { ?>
              <div class="paragraph">
                <?= $text ?>
              </div>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
    </div>
  <?php } ?>
</div>
</section>


<!-- endregion ZineOne's Block -->
