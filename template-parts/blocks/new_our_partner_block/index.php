<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'new_our_partner_block';
$className = 'new_our_partner_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/new_our_partner_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
  <?php if ($title) { ?>
    <div class="block-title headline-1"><?= $title; ?></div>
  <?php } ?>
    <div class="row cards row-cols-2 row-cols-md-3 row-cols-lg-5">
    <?php if (have_rows('cards')) {
      while (have_rows('cards')) {
        the_row();
        $image = get_sub_field('image') ?>
          <div class="col">
            <div class="card">
              <div class="image-wrapper">
                <picture class="card-img aspect-ratio">
                  <img src="<?= $image['url'] ?>"
                       alt="<?= $image['alt'] ?>">
                </picture>
              </div>
              <div class=" learn-more ">
                Read More
                <svg width="17" height="14" viewBox="0 0 17 14" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M0.5 7C0.5 6.73483 0.605345 6.48052 0.792861 6.29302C0.980376 6.10552 1.2347 6.00018 1.49989 6.00018H13.0846L8.79107 1.70896C8.60332 1.52122 8.49784 1.26659 8.49784 1.00108C8.49784 0.735579 8.60332 0.48095 8.79107 0.293211C8.97883 0.105471 9.23347 0 9.49899 0C9.76452 0 10.0192 0.105471 10.2069 0.293211L16.2062 6.29213C16.2994 6.385 16.3732 6.49533 16.4236 6.6168C16.4741 6.73827 16.5 6.86849 16.5 7C16.5 7.13151 16.4741 7.26173 16.4236 7.3832C16.3732 7.50467 16.2994 7.615 16.2062 7.70787L10.2069 13.7068C10.0192 13.8945 9.76452 14 9.49899 14C9.23347 14 8.97883 13.8945 8.79107 13.7068C8.60332 13.5191 8.49784 13.2644 8.49784 12.9989C8.49784 12.7334 8.60332 12.4788 8.79107 12.291L13.0846 7.99982H1.49989C1.2347 7.99982 0.980376 7.89448 0.792861 7.70698C0.605345 7.51948 0.5 7.26517 0.5 7Z"
                        fill="#1174BB"/>
                </svg>
              </div>
            </div>
          </div>
      <?php }
    } ?>
    </div>
  </div>
</section>


<!-- endregion ZineOne's Block -->
