<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'new_testimonials_block';
$className = 'new_testimonials_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/new_testimonials_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

  <div class="container">
    <div class="swiper mySwiper">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <div class="content">
            <picture class="img-container">
              <img src="../../images/testimonials_Kirsten_Edmondson.png"
                   alt="KirstenEdmondson">
            </picture>
            <div class="quote headline-3">
              “Personalization is something that many companies don’t
              understand.
              They think personalization is about making product
              recommendations,
              and it’s not … It’s about creating relevant value-add
              experiences… You
              have to do both — create relevancy and add value at the same
              time. “
            </div>
            <div class="quote-author paragraph">
              - Kirsten Edmondson Wolfe, Sr. Director, AppSource Product
              Marketing,
              <a class="link" href="/#">Microsoft Corp.</a>
            </div>
          </div>

        </div>

      </div>
      <div class="swiper-button-next">
        <svg width="56" height="56" viewBox="0 0 56 56" fill="none"
             xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd"
                d="M20 28C20 27.7348 20.1053 27.4805 20.2929 27.293C20.4804 27.1055 20.7347 27.0002 20.9999 27.0002H32.5846L28.2911 22.709C28.1033 22.5212 27.9978 22.2666 27.9978 22.0011C27.9978 21.7356 28.1033 21.4809 28.2911 21.2932C28.4788 21.1055 28.7335 21 28.999 21C29.2645 21 29.5192 21.1055 29.7069 21.2932L35.7062 27.2921C35.7994 27.385 35.8732 27.4953 35.9236 27.6168C35.9741 27.7383 36 27.8685 36 28C36 28.1315 35.9741 28.2617 35.9236 28.3832C35.8732 28.5047 35.7994 28.615 35.7062 28.7079L29.7069 34.7068C29.5192 34.8945 29.2645 35 28.999 35C28.7335 35 28.4788 34.8945 28.2911 34.7068C28.1033 34.5191 27.9978 34.2644 27.9978 33.9989C27.9978 33.7334 28.1033 33.4788 28.2911 33.291L32.5846 28.9998H20.9999C20.7347 28.9998 20.4804 28.8945 20.2929 28.707C20.1053 28.5195 20 28.2652 20 28Z"
                fill="white"/>
          <rect x="0.5" y="0.5" width="55" height="55" rx="23.5"
                stroke="#BACEDC"/>
        </svg>

      </div>
      <div class="swiper-button-prev">
        <svg width="56" height="56" viewBox="0 0 56 56" fill="none"
             xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd"
                d="M36 28C36 27.7348 35.8947 27.4805 35.7071 27.293C35.5196 27.1055 35.2653 27.0002 35.0001 27.0002H23.4154L27.7089 22.709C27.8967 22.5212 28.0022 22.2666 28.0022 22.0011C28.0022 21.7356 27.8967 21.4809 27.7089 21.2932C27.5212 21.1055 27.2665 21 27.001 21C26.7355 21 26.4808 21.1055 26.2931 21.2932L20.2938 27.2921C20.2006 27.385 20.1268 27.4953 20.0764 27.6168C20.0259 27.7383 20 27.8685 20 28C20 28.1315 20.0259 28.2617 20.0764 28.3832C20.1268 28.5047 20.2006 28.615 20.2938 28.7079L26.2931 34.7068C26.4808 34.8945 26.7355 35 27.001 35C27.2665 35 27.5212 34.8945 27.7089 34.7068C27.8967 34.5191 28.0022 34.2644 28.0022 33.9989C28.0022 33.7334 27.8967 33.4788 27.7089 33.291L23.4154 28.9998H35.0001C35.2653 28.9998 35.5196 28.8945 35.7071 28.707C35.8947 28.5195 36 28.2652 36 28Z"
                fill="white"/>
          <rect x="0.5" y="0.5" width="55" height="55" rx="23.5"
                stroke="#BACEDC"/>
        </svg>


      </div>

    </div>

  </div>
</section>


<!-- endregion ZineOne's Block -->
