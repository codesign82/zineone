<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'form_block';
$className = 'form_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/form_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
  <div class="container">
    <div class="form-wrapper">
      <div class="form-head">
        <h2 class="headline-2">Request a Demo</h2>
        <!--          <hr class="line">-->
        <p class="paragraph">We look forward to getting to know your
          business!</p>
      </div>
      <form class="zineOne-form">
        <div>
          <input class="form-input box-shadow" type="text" name="fullname"
                 placeholder="Full Name">
          <input class="form-input box-shadow" type="email" name="Work Email"
                 placeholder="Work Email">
          <input class="form-input box-shadow" type="text" name="Compant Name"
                 placeholder="Compant Name">
          <input class="form-input box-shadow" type="text" name="Phone Number"
                 placeholder="Phone Number">
        </div>
        <a href="#" class="btn">Submit</a>
      </form>
      <svg class="form-img1" width="218" height="269" viewBox="0 0 218 269" fill="none"
           xmlns="http://www.w3.org/2000/svg">
        <path
          d="M184.009 144.952L189.285 148.181L172.489 172.891L167.213 169.662L184.009 144.952Z"
          fill="#1174BB"/>
        <path
          d="M162.988 152.99L166.239 148.206L193.48 164.878L190.228 169.661L162.988 152.99Z"
          fill="#1174BB"/>
        <path
          d="M128.139 233.729L124.653 229.857L145.399 213.893L148.885 217.764L128.139 233.729Z"
          fill="#32BBA9"/>
        <circle r="10.6238"
                transform="matrix(0.258819 -0.965926 -0.965926 -0.258819 96.5854 154.663)"
                fill="#FE6610"/>
        <path
          d="M200.536 4.74685L194.338 4.47803L194.163 34.254L200.361 34.5228L200.536 4.74685Z"
          fill="#FE6610"/>
        <path
          d="M213.339 23.0958L213.373 17.3311L181.371 15.9438L181.337 21.7085L213.339 23.0958Z"
          fill="#FE6610"/>
        <path
          d="M60.0725 41.3046C62.686 41.4294 64.783 43.5013 64.7563 45.9324C64.7296 48.3635 62.5893 50.2331 59.9759 50.1083C57.3625 49.9835 55.2655 47.9115 55.2922 45.4805C55.3189 43.0494 57.4591 41.1798 60.0725 41.3046Z"
          fill="#A0CD37"/>
        <path
          d="M70.9936 101.101C68.668 99.902 65.8162 100.648 64.6238 102.767C63.4314 104.885 64.35 107.575 66.6756 108.773C69.0011 109.972 71.853 109.226 73.0454 107.107C74.2377 104.988 73.3191 102.299 70.9936 101.101Z"
          fill="#A0CD37"/>
        <path
          d="M182.759 258.559C180.434 257.36 177.582 258.106 176.39 260.225C175.197 262.343 176.116 265.033 178.441 266.231C180.767 267.43 183.619 266.684 184.811 264.565C186.004 262.446 185.085 259.757 182.759 258.559Z"
          fill="#A0CD37"/>
        <circle cx="13.0115" cy="66.8389" r="10.6238"
                transform="rotate(-75 13.0115 66.8389)" fill="#FE6610"/>
        <path
          d="M127.757 25.9239L125.938 20.3423L95.3451 27.9155L97.1642 33.4971L127.757 25.9239Z"
          fill="#32BBA9"/>
        <path
          d="M113.27 42.0781L119.192 40.6118L109.797 11.7826L103.874 13.2489L113.27 42.0781Z"
          fill="#32BBA9"/>
        <path
          d="M73.985 194.932L67.8016 195.09L69.9921 224.888L76.1755 224.729L73.985 194.932Z"
          fill="#1174BB"/>
        <path
          d="M88.1715 212.404L87.7474 206.635L55.82 207.453L56.2441 213.222L88.1715 212.404Z"
          fill="#1174BB"/>
      </svg>
      <svg class="form-img2" width="220" height="253" viewBox="0 0 220 253" fill="none"
           xmlns="http://www.w3.org/2000/svg">
        <path
          d="M193.69 79.1694L198.567 82.1606L183.04 105.052L178.162 102.061L193.69 79.1694Z"
          fill="#1174BB"/>
        <path
          d="M174.256 86.6159L177.262 82.1841L202.446 97.6288L199.44 102.061L174.256 86.6159Z"
          fill="#1174BB"/>
        <path
          d="M7.2461 164.607L11.2826 160.971L32.2756 181.218L28.2391 184.854L7.2461 164.607Z"
          fill="#32BBA9"/>
        <path
          d="M11.3783 184.279L7.31384 180.359L28.1628 161.581L32.2273 165.501L11.3783 184.279Z"
          fill="#32BBA9"/>
        <path
          d="M151.205 154.852L147.982 151.266L167.162 136.476L170.385 140.062L151.205 154.852Z"
          fill="#32BBA9"/>
        <ellipse rx="9.84069" ry="9.82313"
                 transform="matrix(0.258322 -0.966059 -0.965792 -0.259317 97.1516 185.274)"
                 fill="#FE6610"/>
        <ellipse rx="9.84069" ry="9.82313"
                 transform="matrix(0.258322 -0.966059 -0.965792 -0.259317 192.75 172.151)"
                 fill="#FE6610"/>
        <path
          d="M147.42 16.2075L141.689 15.9585L141.528 43.5433L147.258 43.7923L147.42 16.2075Z"
          fill="#FE6610"/>
        <path
          d="M159.256 33.2062L159.288 27.8657L129.702 26.5805L129.67 31.921L159.256 33.2062Z"
          fill="#FE6610"/>
        <path
          d="M75.9555 2.86344L79.6688 0L96.3165 18.5234L92.6032 21.3868L75.9555 2.86344Z"
          fill="#32BBA9"/>
        <path
          d="M17.2457 27.928C15.6773 29.5821 15.8962 32.1658 17.7347 33.699C19.5732 35.2322 22.335 35.1342 23.9034 33.4802C25.4718 31.8262 25.2529 29.2425 23.4144 27.7093C21.576 26.1761 18.8142 26.274 17.2457 27.928Z"
          fill="#1174BB"/>
        <path
          d="M72.2477 244.453C70.6793 246.107 70.8982 248.691 72.7366 250.224C74.5751 251.757 77.3369 251.659 78.9054 250.005C80.4738 248.351 80.2549 245.767 78.4164 244.234C76.5779 242.701 73.8161 242.799 72.2477 244.453Z"
          fill="#1174BB"/>
        <path
          d="M70.7449 142.423C68.5949 141.313 65.9584 142.004 64.856 143.966C63.7536 145.929 64.6029 148.421 66.7529 149.531C68.9029 150.641 71.5394 149.95 72.6418 147.988C73.7441 146.025 72.8949 143.533 70.7449 142.423Z"
          fill="#A0CD37"/>
        <path
          d="M216.107 38.7541C213.957 37.6437 211.321 38.3347 210.218 40.2975C209.116 42.2603 209.965 44.7516 212.115 45.862C214.265 46.9724 216.902 46.2814 218.004 44.3186C219.106 42.3558 218.257 39.8645 216.107 38.7541Z"
          fill="#A0CD37"/>
      </svg>
    </div>
  </div>
</section>


<!-- endregion ZineOne's Block -->
