<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'new_additional_usecases_block';
$className = 'new_additional_usecases_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/new_additional_usecases_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');

?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>

<div class="container">
  <?php if ($title) { ?>
    <h2 class="headline-2"><?= $title; ?></h2>
  <?php } ?>
  <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4">
    <?php if (have_rows('card')) {
      while (have_rows('card')) {
        the_row();
        $svg = get_sub_field('svg');
        $subtitle = get_sub_field('subtitle');
        ?>
        <div class="col">
          <div class="additional-use-group box-shadow">
            <div class="why-choose_img svg_border-color">
              <?= $svg; ?>
            </div>
            <h2 class="headline-3"><?= $subtitle; ?></h2>
          </div>
        </div>
      <?php }
    } ?>
  </div>
</div>
</section>


<!-- endregion ZineOne's Block -->
