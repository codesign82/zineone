<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'businesses_block';
$className = 'businesses_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/businesses_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$description = get_field( 'description' );
$link        = get_field( 'link' );
$logos       = get_field( 'logos' );
?>
<!-- region ZineOne's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="businesses">
    <div class="businesses-head">
      <?php if ( $title ): ?>
        <h2 class="headline-2 bottom-border iv-st-from-bottom"><?= $title ?></h2><?php endif; ?>
      <?php if ( $description ): ?>
        <div class="paragraph iv-st-from-bottom"><?= $description ?></div><?php endif; ?>
      <?php if ( $link ): ?>
        <a href="<?= $link['url'] ?>" title="<?= $link['title'] ?>" target="<?= $link['target'] ?>" class="btn"><?= $link['title'] ?></a><?php endif; ?>
    </div>
    <?php if ( have_rows('logos') ): ?>
      <div class="businesses-images box-shadow iv-st-from-bottom">
        <?php while ( have_rows('logos') ): the_row(); $logo = get_sub_field('logo'); ?>
          <picture class="businesses_img iv-st-from-bottom">
            <img src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>">
          </picture>
        <?php endwhile; ?>
      </div>
    <?php endif; ?>
  </div>
</div>
</section>


<!-- endregion ZineOne's Block -->
