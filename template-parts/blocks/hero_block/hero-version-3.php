<?php
/****************************
 *     Custom ACF Meta      *
 ****************************/
$hero_version_3 = get_field( 'hero_version_3' );
$title          = $hero_version_3['title'];
$subtitle       = $hero_version_3['subtitle']; ?>
<?php if ( $title ) { ?>
  <h6 class="left-hero-content-text"><?= $title ?></h6>
<?php } ?>
<?php if ( $subtitle ) { ?>
  <div class="headline-1"><?= $subtitle ?></div>
<?php } ?>
<?php
