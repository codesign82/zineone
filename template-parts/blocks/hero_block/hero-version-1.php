<?php
/****************************
 *     Custom ACF Meta      *
 ****************************/
$hero_version_1 = get_field( 'hero_version_1' );
$title          = $hero_version_1['title'];
$description    = $hero_version_1['description'];
$cta_button     = $hero_version_1['cta_button']; ?>
  <div class="left-hero-content">
    <?php if ( $title ) { ?>
      <h1 class="headline-1 iv-st-from-bottom"><?= $title ?></h1>
    <?php } ?>
    <?php if ( $description ) { ?>
      <div class="paragraph iv-st-from-bottom"><?= $description ?></div>
    <?php } ?>
    <?php if ( $cta_button ) { ?>
      <a class="btn iv-st-from-bottom" href="<?= $cta_button['url'] ?>" target="<?= $cta_button['target'] ?>"><?= $cta_button['title'] ?></a>
    <?php } ?>
  </div>
  <div class="right-hero-content">
    <svg viewBox="0 0 650 650">
      <defs>
        <linearGradient id="a" x1="11" y1="446" x2="629" y2="446"
                        gradientTransform="translate(0 -126)"
                        gradientUnits="userSpaceOnUse">
          <stop offset="0" stop-color="#8d5cf6"/>
          <stop offset="1" stop-color="#423b7e"/>
        </linearGradient>
        <linearGradient id="b" x1="176542" y1="446" x2="267507" y2="446"
                        gradientTransform="translate(0 -126)"
                        gradientUnits="userSpaceOnUse">
          <stop offset="0" stop-color="#d4088c"/>
          <stop offset="0.23" stop-color="#d4088c"/>
          <stop offset="0.51" stop-color="#6ee1f8"/>
          <stop offset="0.78" stop-color="#6ee8fc"/>
          <stop offset="1" stop-color="#6ee8fc"/>
        </linearGradient>
        <linearGradient id="c" x1="162370" x2="246020" xlink:href="#b"/>
        <linearGradient id="d" x1="145734" x2="220796" xlink:href="#b"/>
        <linearGradient id="e" x1="100" x2="540" xlink:href="#a"/>
        <clipPath id="f">
          <circle cx="320" cy="320" r="235" fill="none"/>
        </clipPath>
        <linearGradient id="g" x1="242.25" y1="562.15" x2="216.48"
                        y2="560.81" gradientTransform="translate(0 -126)"
                        gradientUnits="userSpaceOnUse">
          <stop offset="0" stop-color="#32bba9"/>
          <stop offset="1" stop-color="#1174bb"/>
        </linearGradient>
        <linearGradient id="h" x1="242.71" y1="553.37" x2="216.93"
                        y2="552.04" xlink:href="#g"/>
        <linearGradient id="i" x1="243.07" y1="546.39" x2="217.29"
                        y2="545.05" xlink:href="#g"/>
        <linearGradient id="j" x1="243.44" y1="539.31" x2="217.66"
                        y2="537.97" xlink:href="#g"/>
        <linearGradient id="k" x1="243.8" y1="532.23" x2="218.03"
                        y2="530.89" xlink:href="#g"/>
        <linearGradient id="l" x1="244.34" y1="521.86" x2="218.57"
                        y2="520.52" xlink:href="#g"/>
        <linearGradient id="m" x1="244.8" y1="513.08" x2="219.02"
                        y2="511.74" xlink:href="#g"/>
        <linearGradient id="n" x1="245.34" y1="502.61" x2="219.57"
                        y2="501.27" xlink:href="#g"/>
        <linearGradient id="o" x1="245.8" y1="493.83" x2="220.02"
                        y2="492.5" xlink:href="#g"/>
        <linearGradient id="p" x1="246.34" y1="483.36" x2="220.57"
                        y2="482.02" xlink:href="#g"/>
        <linearGradient id="q" x1="246.8" y1="474.59" x2="221.02"
                        y2="473.25" xlink:href="#g"/>
        <linearGradient id="r" x1="247.33" y1="464.21" x2="221.56"
                        y2="462.88" xlink:href="#g"/>
        <linearGradient id="s" x1="247.98" y1="451.85" x2="222.2"
                        y2="450.51" xlink:href="#g"/>
        <linearGradient id="t" x1="248.43" y1="443.07" x2="222.66"
                        y2="441.73" xlink:href="#g"/>
        <linearGradient id="u" x1="248.97" y1="432.7" x2="223.2"
                        y2="431.36" xlink:href="#g"/>
        <linearGradient id="v" x1="249.43" y1="423.82" x2="223.66"
                        y2="422.49" xlink:href="#g"/>
        <linearGradient id="w" x1="249.97" y1="413.45" x2="224.19"
                        y2="412.11" xlink:href="#g"/>
        <linearGradient id="x" x1="250.42" y1="404.67" x2="224.65"
                        y2="403.34" xlink:href="#g"/>
        <linearGradient id="y" x1="250.97" y1="394.2" x2="225.19"
                        y2="392.86" xlink:href="#g"/>
        <linearGradient id="z" x1="251.6" y1="381.94" x2="225.83"
                        y2="380.6" xlink:href="#g"/>
        <linearGradient id="aa" x1="252.24" y1="369.67" x2="226.47"
                        y2="368.33" xlink:href="#g"/>
        <linearGradient id="ab" x1="252.7" y1="360.89" x2="226.92"
                        y2="359.55" xlink:href="#g"/>
        <linearGradient id="ac" x1="253.24" y1="350.42" x2="227.47"
                        y2="349.08" xlink:href="#g"/>
        <linearGradient id="ad" x1="253.7" y1="341.64" x2="227.92"
                        y2="340.31" xlink:href="#g"/>
        <linearGradient id="ae" x1="254.24" y1="331.17" x2="228.47"
                        y2="329.83" xlink:href="#g"/>
        <linearGradient id="af" x1="254.7" y1="322.4" x2="228.92"
                        y2="321.06" xlink:href="#g"/>
        <linearGradient id="ag" x1="318.5" y1="567.1" x2="318.5"
                        y2="318.91" xlink:href="#a"/>
        <linearGradient id="ah" x1="318.5" y1="567.09" x2="318.5"
                        y2="318.9" xlink:href="#a"/>
        <linearGradient id="bg" x1="435.5" y1="567.1" x2="435.5"
                        y2="318.91" xlink:href="#a"/>
        <linearGradient id="bh" x1="435.5" y1="567.09" x2="435.5"
                        y2="318.9" xlink:href="#a"/>
        <linearGradient id="cg" x1="269.42" y1="629.44" x2="243.79"
                        y2="627.08" xlink:href="#g"/>
        <linearGradient id="ch" x1="270.22" y1="620.71" x2="244.59"
                        y2="618.36" xlink:href="#g"/>
        <linearGradient id="ci" x1="270.86" y1="613.77" x2="245.23"
                        y2="611.42" xlink:href="#g"/>
        <linearGradient id="cj" x1="271.5" y1="606.73" x2="245.88"
                        y2="604.37" xlink:href="#g"/>
        <linearGradient id="ck" x1="272.15" y1="599.69" x2="246.52"
                        y2="597.33" xlink:href="#g"/>
        <linearGradient id="cl" x1="273.1" y1="589.37" x2="247.47"
                        y2="587.02" xlink:href="#g"/>
        <linearGradient id="cm" x1="273.9" y1="580.65" x2="248.27"
                        y2="578.29" xlink:href="#g"/>
        <linearGradient id="cn" x1="274.85" y1="570.23" x2="249.23"
                        y2="567.88" xlink:href="#g"/>
        <linearGradient id="co" x1="275.65" y1="561.51" x2="250.03"
                        y2="559.16" xlink:href="#g"/>
        <linearGradient id="cp" x1="276.61" y1="551.09" x2="250.98"
                        y2="548.74" xlink:href="#g"/>
        <linearGradient id="cq" x1="277.41" y1="542.37" x2="251.78"
                        y2="540.02" xlink:href="#g"/>
        <linearGradient id="cr" x1="278.36" y1="532.06" x2="252.73"
                        y2="529.7" xlink:href="#g"/>
        <linearGradient id="cs" x1="279.49" y1="519.76" x2="253.86"
                        y2="517.41" xlink:href="#g"/>
        <linearGradient id="ct" x1="280.29" y1="511.03" x2="254.66"
                        y2="508.68" xlink:href="#g"/>
        <linearGradient id="cu" x1="281.23" y1="500.72" x2="255.61"
                        y2="498.37" xlink:href="#g"/>
        <linearGradient id="cv" x1="355.42" y1="629.44" x2="329.79"
                        y2="627.08" xlink:href="#g"/>
        <linearGradient id="cw" x1="356.22" y1="620.71" x2="330.59"
                        y2="618.36" xlink:href="#g"/>
        <linearGradient id="cx" x1="356.86" y1="613.77" x2="331.23"
                        y2="611.42" xlink:href="#g"/>
        <linearGradient id="cy" x1="357.5" y1="606.73" x2="331.88"
                        y2="604.37" xlink:href="#g"/>
        <linearGradient id="cz" x1="358.15" y1="599.69" x2="332.52"
                        y2="597.33" xlink:href="#g"/>
        <linearGradient id="da" x1="359.1" y1="589.37" x2="333.47"
                        y2="587.02" xlink:href="#g"/>
        <linearGradient id="db" x1="359.9" y1="580.65" x2="334.27"
                        y2="578.29" xlink:href="#g"/>
        <linearGradient id="dc" x1="360.85" y1="570.23" x2="335.23"
                        y2="567.88" xlink:href="#g"/>
        <linearGradient id="dd" x1="361.65" y1="561.51" x2="336.03"
                        y2="559.16" xlink:href="#g"/>
        <linearGradient id="de" x1="362.61" y1="551.09" x2="336.98"
                        y2="548.74" xlink:href="#g"/>
        <linearGradient id="df" x1="363.41" y1="542.37" x2="337.78"
                        y2="540.02" xlink:href="#g"/>
        <linearGradient id="dg" x1="364.36" y1="532.06" x2="338.73"
                        y2="529.7" xlink:href="#g"/>
        <linearGradient id="dh" x1="365.49" y1="519.76" x2="339.86"
                        y2="517.41" xlink:href="#g"/>
        <linearGradient id="di" x1="366.29" y1="511.03" x2="340.66"
                        y2="508.68" xlink:href="#g"/>
        <linearGradient id="dj" x1="367.23" y1="500.72" x2="341.61"
                        y2="498.37" xlink:href="#g"/>
        <linearGradient id="dk" x1="413.42" y1="426.44" x2="387.79"
                        y2="424.08" xlink:href="#g"/>
        <linearGradient id="dl" x1="414.22" y1="417.71" x2="388.59"
                        y2="415.36" xlink:href="#g"/>
        <linearGradient id="dm" x1="414.86" y1="410.77" x2="389.23"
                        y2="408.42" xlink:href="#g"/>
        <linearGradient id="dn" x1="415.5" y1="403.73" x2="389.88"
                        y2="401.37" xlink:href="#g"/>
        <linearGradient id="do" x1="416.15" y1="396.69" x2="390.52"
                        y2="394.33" xlink:href="#g"/>
        <linearGradient id="dp" x1="417.1" y1="386.37" x2="391.47"
                        y2="384.02" xlink:href="#g"/>
        <linearGradient id="dq" x1="417.9" y1="377.65" x2="392.27"
                        y2="375.29" xlink:href="#g"/>
        <linearGradient id="dr" x1="418.85" y1="367.23" x2="393.23"
                        y2="364.88" xlink:href="#g"/>
        <linearGradient id="ds" x1="419.65" y1="358.51" x2="394.03"
                        y2="356.16" xlink:href="#g"/>
        <linearGradient id="dt" x1="420.61" y1="348.09" x2="394.98"
                        y2="345.74" xlink:href="#g"/>
        <linearGradient id="du" x1="421.41" y1="339.37" x2="395.78"
                        y2="337.02" xlink:href="#g"/>
        <linearGradient id="dv" x1="422.36" y1="329.06" x2="396.73"
                        y2="326.7" xlink:href="#g"/>
        <linearGradient id="dw" x1="423.49" y1="316.76" x2="397.86"
                        y2="314.41" xlink:href="#g"/>
        <linearGradient id="dx" x1="424.29" y1="308.03" x2="398.66"
                        y2="305.68" xlink:href="#g"/>
        <linearGradient id="dy" x1="425.23" y1="297.72" x2="399.61"
                        y2="295.37" xlink:href="#g"/>
        <linearGradient id="dz" x1="472.42" y1="629.44" x2="446.79"
                        y2="627.08" xlink:href="#g"/>
        <linearGradient id="ea" x1="473.22" y1="620.71" x2="447.59"
                        y2="618.36" xlink:href="#g"/>
        <linearGradient id="eb" x1="473.86" y1="613.77" x2="448.23"
                        y2="611.42" xlink:href="#g"/>
        <linearGradient id="ec" x1="474.5" y1="606.73" x2="448.88"
                        y2="604.37" xlink:href="#g"/>
        <linearGradient id="ed" x1="475.15" y1="599.69" x2="449.52"
                        y2="597.33" xlink:href="#g"/>
        <linearGradient id="ee" x1="476.1" y1="589.37" x2="450.47"
                        y2="587.02" xlink:href="#g"/>
        <linearGradient id="ef" x1="476.9" y1="580.65" x2="451.27"
                        y2="578.29" xlink:href="#g"/>
        <linearGradient id="eg" x1="477.85" y1="570.23" x2="452.23"
                        y2="567.88" xlink:href="#g"/>
        <linearGradient id="eh" x1="478.65" y1="561.51" x2="453.03"
                        y2="559.16" xlink:href="#g"/>
        <linearGradient id="ei" x1="479.61" y1="551.09" x2="453.98"
                        y2="548.74" xlink:href="#g"/>
        <linearGradient id="ej" x1="480.41" y1="542.37" x2="454.78"
                        y2="540.02" xlink:href="#g"/>
        <linearGradient id="ek" x1="481.36" y1="532.06" x2="455.73"
                        y2="529.7" xlink:href="#g"/>
        <linearGradient id="el" x1="482.49" y1="519.76" x2="456.86"
                        y2="517.41" xlink:href="#g"/>
        <linearGradient id="em" x1="483.29" y1="511.03" x2="457.66"
                        y2="508.68" xlink:href="#g"/>
        <linearGradient id="en" x1="484.23" y1="500.72" x2="458.61"
                        y2="498.37" xlink:href="#g"/>
        <linearGradient id="eo" x1="297.42" y1="573.44" x2="271.79"
                        y2="571.08" gradientTransform="translate(0 -126)"
                        gradientUnits="userSpaceOnUse">
          <stop offset="0" stop-color="#fb762b"/>
          <stop offset="1" stop-color="#f2512d"/>
        </linearGradient>
        <linearGradient id="ep" x1="298.22" y1="564.71" x2="272.59"
                        y2="562.36" xlink:href="#eo"/>
        <linearGradient id="eq" x1="298.86" y1="557.77" x2="273.23"
                        y2="555.42" xlink:href="#eo"/>
        <linearGradient id="er" x1="299.5" y1="550.73" x2="273.88"
                        y2="548.37" xlink:href="#eo"/>
        <linearGradient id="es" x1="300.15" y1="543.69" x2="274.52"
                        y2="541.33" xlink:href="#eo"/>
        <linearGradient id="et" x1="301.1" y1="533.37" x2="275.47"
                        y2="531.02" xlink:href="#eo"/>
        <linearGradient id="eu" x1="301.9" y1="524.65" x2="276.27"
                        y2="522.29" xlink:href="#eo"/>
        <linearGradient id="ev" x1="302.85" y1="514.23" x2="277.23"
                        y2="511.88" xlink:href="#eo"/>
        <linearGradient id="ew" x1="303.65" y1="505.51" x2="278.03"
                        y2="503.16" xlink:href="#eo"/>
        <linearGradient id="ex" x1="304.61" y1="495.09" x2="278.98"
                        y2="492.74" xlink:href="#eo"/>
        <linearGradient id="ey" x1="305.41" y1="486.37" x2="279.78"
                        y2="484.02" xlink:href="#eo"/>
        <linearGradient id="ez" x1="306.36" y1="476.06" x2="280.73"
                        y2="473.7" xlink:href="#eo"/>
        <linearGradient id="fa" x1="307.49" y1="463.76" x2="281.86"
                        y2="461.41" xlink:href="#eo"/>
        <linearGradient id="fb" x1="308.29" y1="455.03" x2="282.66"
                        y2="452.68" xlink:href="#eo"/>
        <linearGradient id="fc" x1="309.23" y1="444.72" x2="283.61"
                        y2="442.37" xlink:href="#eo"/>
        <linearGradient id="fd" x1="414.42" y1="573.44" x2="388.79"
                        y2="571.08" xlink:href="#eo"/>
        <linearGradient id="fe" x1="415.22" y1="564.71" x2="389.59"
                        y2="562.36" xlink:href="#eo"/>
        <linearGradient id="ff" x1="415.86" y1="557.77" x2="390.23"
                        y2="555.42" xlink:href="#eo"/>
        <linearGradient id="fg" x1="416.5" y1="550.73" x2="390.88"
                        y2="548.37" xlink:href="#eo"/>
        <linearGradient id="fh" x1="417.15" y1="543.69" x2="391.52"
                        y2="541.33" xlink:href="#eo"/>
        <linearGradient id="fi" x1="418.1" y1="533.37" x2="392.47"
                        y2="531.02" xlink:href="#eo"/>
        <linearGradient id="fj" x1="418.9" y1="524.65" x2="393.27"
                        y2="522.29" xlink:href="#eo"/>
        <linearGradient id="fk" x1="419.85" y1="514.23" x2="394.23"
                        y2="511.88" xlink:href="#eo"/>
        <linearGradient id="fl" x1="420.65" y1="505.51" x2="395.03"
                        y2="503.16" xlink:href="#eo"/>
        <linearGradient id="fm" x1="421.61" y1="495.09" x2="395.98"
                        y2="492.74" xlink:href="#eo"/>
        <linearGradient id="fn" x1="422.41" y1="486.37" x2="396.78"
                        y2="484.02" xlink:href="#eo"/>
        <linearGradient id="fo" x1="423.36" y1="476.06" x2="397.73"
                        y2="473.7" xlink:href="#eo"/>
        <linearGradient id="fp" x1="424.49" y1="463.76" x2="398.86"
                        y2="461.41" xlink:href="#eo"/>
        <linearGradient id="fq" x1="425.29" y1="455.03" x2="399.66"
                        y2="452.68" xlink:href="#eo"/>
        <linearGradient id="fr" x1="426.23" y1="444.72" x2="400.61"
                        y2="442.37" xlink:href="#eo"/>
        <linearGradient id="fs" x1="383.17" y1="576.81" x2="357.54"
                        y2="574.47" xlink:href="#eo"/>
        <linearGradient id="ft" x1="383.8" y1="569.87" x2="358.17"
                        y2="567.53" xlink:href="#eo"/>
        <linearGradient id="fu" x1="384.45" y1="562.83" x2="358.82"
                        y2="560.49" xlink:href="#eo"/>
        <linearGradient id="fv" x1="385.08" y1="555.88" x2="359.45"
                        y2="553.54" xlink:href="#eo"/>
        <linearGradient id="fw" x1="386.03" y1="545.47" x2="360.4"
                        y2="543.13" xlink:href="#eo"/>
        <linearGradient id="fx" x1="386.83" y1="536.74" x2="361.2"
                        y2="534.4" xlink:href="#eo"/>
        <linearGradient id="fy" x1="387.78" y1="526.33" x2="362.15"
                        y2="523.99" xlink:href="#eo"/>
        <linearGradient id="fz" x1="388.57" y1="517.6" x2="362.94"
                        y2="515.26" xlink:href="#eo"/>
        <linearGradient id="ga" x1="389.52" y1="507.29" x2="363.89"
                        y2="504.95" xlink:href="#eo"/>
        <linearGradient id="gb" x1="390.32" y1="498.46" x2="364.69"
                        y2="496.12" xlink:href="#eo"/>
        <linearGradient id="gc" x1="391.26" y1="488.15" x2="365.63"
                        y2="485.81" xlink:href="#eo"/>
        <linearGradient id="gd" x1="392.38" y1="475.95" x2="366.75"
                        y2="473.61" xlink:href="#eo"/>
        <linearGradient id="ge" x1="393.18" y1="467.12" x2="367.55"
                        y2="464.78" xlink:href="#eo"/>
        <linearGradient id="gf" x1="394.13" y1="456.81" x2="368.5"
                        y2="454.47" xlink:href="#eo"/>
        <linearGradient id="gg" x1="395.24" y1="444.61" x2="369.61"
                        y2="442.27" xlink:href="#eo"/>
        <linearGradient id="gh" x1="269.42" y1="471.44" x2="243.79"
                        y2="469.08" gradientTransform="translate(0 -126)"
                        gradientUnits="userSpaceOnUse">
          <stop offset="0" stop-color="#a0cd37"/>
          <stop offset="1" stop-color="#32bba9"/>
        </linearGradient>
        <linearGradient id="gi" x1="270.22" y1="462.71" x2="244.59"
                        y2="460.36" xlink:href="#gh"/>
        <linearGradient id="gj" x1="270.86" y1="455.77" x2="245.23"
                        y2="453.42" xlink:href="#gh"/>
        <linearGradient id="gk" x1="271.5" y1="448.73" x2="245.88"
                        y2="446.37" xlink:href="#gh"/>
        <linearGradient id="gl" x1="272.15" y1="441.69" x2="246.52"
                        y2="439.33" xlink:href="#gh"/>
        <linearGradient id="gm" x1="273.1" y1="431.37" x2="247.47"
                        y2="429.02" xlink:href="#gh"/>
        <linearGradient id="gn" x1="273.9" y1="422.65" x2="248.27"
                        y2="420.29" xlink:href="#gh"/>
        <linearGradient id="go" x1="274.85" y1="412.23" x2="249.23"
                        y2="409.88" xlink:href="#gh"/>
        <linearGradient id="gp" x1="275.65" y1="403.51" x2="250.03"
                        y2="401.16" xlink:href="#gh"/>
        <linearGradient id="gq" x1="276.61" y1="393.09" x2="250.98"
                        y2="390.74" xlink:href="#gh"/>
        <linearGradient id="gr" x1="277.41" y1="384.37" x2="251.78"
                        y2="382.02" xlink:href="#gh"/>
        <linearGradient id="gs" x1="278.36" y1="374.06" x2="252.73"
                        y2="371.7" xlink:href="#gh"/>
        <linearGradient id="gt" x1="279.49" y1="361.76" x2="253.86"
                        y2="359.41" xlink:href="#gh"/>
        <linearGradient id="gu" x1="280.29" y1="353.03" x2="254.66"
                        y2="350.68" xlink:href="#gh"/>
        <linearGradient id="gv" x1="281.23" y1="342.72" x2="255.61"
                        y2="340.37" xlink:href="#gh"/>
        <linearGradient id="gw" x1="355.42" y1="471.44" x2="329.79"
                        y2="469.08" xlink:href="#gh"/>
        <linearGradient id="gx" x1="356.22" y1="462.71" x2="330.59"
                        y2="460.36" xlink:href="#gh"/>
        <linearGradient id="gy" x1="356.86" y1="455.77" x2="331.23"
                        y2="453.42" xlink:href="#gh"/>
        <linearGradient id="gz" x1="357.5" y1="448.73" x2="331.88"
                        y2="446.37" xlink:href="#gh"/>
        <linearGradient id="ha" x1="358.15" y1="441.69" x2="332.52"
                        y2="439.33" xlink:href="#gh"/>
        <linearGradient id="hb" x1="359.1" y1="431.37" x2="333.47"
                        y2="429.02" xlink:href="#gh"/>
        <linearGradient id="hc" x1="359.9" y1="422.65" x2="334.27"
                        y2="420.29" xlink:href="#gh"/>
        <linearGradient id="hd" x1="360.85" y1="412.23" x2="335.23"
                        y2="409.88" xlink:href="#gh"/>
        <linearGradient id="he" x1="361.65" y1="403.51" x2="336.03"
                        y2="401.16" xlink:href="#gh"/>
        <linearGradient id="hf" x1="362.61" y1="393.09" x2="336.98"
                        y2="390.74" xlink:href="#gh"/>
        <linearGradient id="hg" x1="363.41" y1="384.37" x2="337.78"
                        y2="382.02" xlink:href="#gh"/>
        <linearGradient id="hh" x1="364.36" y1="374.06" x2="338.73"
                        y2="371.7" xlink:href="#gh"/>
        <linearGradient id="hi" x1="365.49" y1="361.76" x2="339.86"
                        y2="359.41" xlink:href="#gh"/>
        <linearGradient id="hj" x1="366.29" y1="353.03" x2="340.66"
                        y2="350.68" xlink:href="#gh"/>
        <linearGradient id="hk" x1="367.23" y1="342.72" x2="341.61"
                        y2="340.37" xlink:href="#gh"/>
        <linearGradient id="hl" x1="472.42" y1="471.44" x2="446.79"
                        y2="469.08" xlink:href="#gh"/>
        <linearGradient id="hm" x1="473.22" y1="462.71" x2="447.59"
                        y2="460.36" xlink:href="#gh"/>
        <linearGradient id="hn" x1="473.86" y1="455.77" x2="448.23"
                        y2="453.42" xlink:href="#gh"/>
        <linearGradient id="ho" x1="474.5" y1="448.73" x2="448.88"
                        y2="446.37" xlink:href="#gh"/>
        <linearGradient id="hp" x1="475.15" y1="441.69" x2="449.52"
                        y2="439.33" xlink:href="#gh"/>
        <linearGradient id="hq" x1="476.1" y1="431.37" x2="450.47"
                        y2="429.02" xlink:href="#gh"/>
        <linearGradient id="hr" x1="476.9" y1="422.65" x2="451.27"
                        y2="420.29" xlink:href="#gh"/>
        <linearGradient id="hs" x1="477.85" y1="412.23" x2="452.23"
                        y2="409.88" xlink:href="#gh"/>
        <linearGradient id="ht" x1="478.65" y1="403.51" x2="453.03"
                        y2="401.16" xlink:href="#gh"/>
        <linearGradient id="hu" x1="479.61" y1="393.09" x2="453.98"
                        y2="390.74" xlink:href="#gh"/>
        <linearGradient id="hv" x1="480.41" y1="384.37" x2="454.78"
                        y2="382.02" xlink:href="#gh"/>
        <linearGradient id="hw" x1="481.36" y1="374.06" x2="455.73"
                        y2="371.7" xlink:href="#gh"/>
        <linearGradient id="hx" x1="482.49" y1="361.76" x2="456.86"
                        y2="359.41" xlink:href="#gh"/>
        <linearGradient id="hy" x1="483.29" y1="353.03" x2="457.66"
                        y2="350.68" xlink:href="#gh"/>
        <linearGradient id="hz" x1="484.23" y1="342.72" x2="458.61"
                        y2="340.37" xlink:href="#gh"/>
        <linearGradient id="ia" x1="169.9" y1="567.1" x2="169.9"
                        y2="318.91" xlink:href="#a"/>
        <linearGradient id="ib" x1="169.9" y1="567.09" x2="169.9"
                        y2="318.9" xlink:href="#a"/>
        <linearGradient id="ja" x1="206.82" y1="629.44" x2="181.19"
                        y2="627.08" xlink:href="#g"/>
        <linearGradient id="jb" x1="207.62" y1="620.71" x2="181.99"
                        y2="618.36" xlink:href="#g"/>
        <linearGradient id="jc" x1="208.26" y1="613.77" x2="182.63"
                        y2="611.42" xlink:href="#g"/>
        <linearGradient id="jd" x1="208.9" y1="606.73" x2="183.28"
                        y2="604.37" xlink:href="#g"/>
        <linearGradient id="je" x1="209.55" y1="599.69" x2="183.92"
                        y2="597.33" xlink:href="#g"/>
        <linearGradient id="jf" x1="210.5" y1="589.37" x2="184.87"
                        y2="587.02" xlink:href="#g"/>
        <linearGradient id="jg" x1="211.3" y1="580.65" x2="185.67"
                        y2="578.29" xlink:href="#g"/>
        <linearGradient id="jh" x1="212.25" y1="570.23" x2="186.63"
                        y2="567.88" xlink:href="#g"/>
        <linearGradient id="ji" x1="213.05" y1="561.51" x2="187.43"
                        y2="559.16" xlink:href="#g"/>
        <linearGradient id="jj" x1="214.01" y1="551.09" x2="188.38"
                        y2="548.74" xlink:href="#g"/>
        <linearGradient id="jk" x1="214.81" y1="542.37" x2="189.18"
                        y2="540.02" xlink:href="#g"/>
        <linearGradient id="jl" x1="215.76" y1="532.06" x2="190.13"
                        y2="529.7" xlink:href="#g"/>
        <linearGradient id="jm" x1="216.89" y1="519.76" x2="191.26"
                        y2="517.41" xlink:href="#g"/>
        <linearGradient id="jn" x1="217.69" y1="511.03" x2="192.06"
                        y2="508.68" xlink:href="#g"/>
        <linearGradient id="jo" x1="218.63" y1="500.72" x2="193.01"
                        y2="498.37" xlink:href="#g"/>
        <linearGradient id="jp" x1="206.82" y1="471.44" x2="181.19"
                        y2="469.08" xlink:href="#gh"/>
        <linearGradient id="jq" x1="207.62" y1="462.71" x2="181.99"
                        y2="460.36" xlink:href="#gh"/>
        <linearGradient id="jr" x1="208.26" y1="455.77" x2="182.63"
                        y2="453.42" xlink:href="#gh"/>
        <linearGradient id="js" x1="208.9" y1="448.73" x2="183.28"
                        y2="446.37" xlink:href="#gh"/>
        <linearGradient id="jt" x1="209.55" y1="441.69" x2="183.92"
                        y2="439.33" xlink:href="#gh"/>
        <linearGradient id="ju" x1="210.5" y1="431.37" x2="184.87"
                        y2="429.02" xlink:href="#gh"/>
        <linearGradient id="jv" x1="211.3" y1="422.65" x2="185.67"
                        y2="420.29" xlink:href="#gh"/>
        <linearGradient id="jw" x1="212.25" y1="412.23" x2="186.63"
                        y2="409.88" xlink:href="#gh"/>
        <linearGradient id="jx" x1="213.05" y1="403.51" x2="187.43"
                        y2="401.16" xlink:href="#gh"/>
        <linearGradient id="jy" x1="214.01" y1="393.09" x2="188.38"
                        y2="390.74" xlink:href="#gh"/>
        <linearGradient id="jz" x1="214.81" y1="384.37" x2="189.18"
                        y2="382.02" xlink:href="#gh"/>
        <linearGradient id="ka" x1="215.76" y1="374.06" x2="190.13"
                        y2="371.7" xlink:href="#gh"/>
        <linearGradient id="kb" x1="216.89" y1="361.76" x2="191.26"
                        y2="359.41" xlink:href="#gh"/>
        <linearGradient id="kc" x1="217.69" y1="353.03" x2="192.06"
                        y2="350.68" xlink:href="#gh"/>
        <linearGradient id="kd" x1="218.63" y1="342.72" x2="193.01"
                        y2="340.37" xlink:href="#gh"/>
        <linearGradient id="ke" x1="297.42" y1="415.44" x2="271.79"
                        y2="413.08" xlink:href="#gh"/>
        <linearGradient id="kf" x1="298.22" y1="406.71" x2="272.59"
                        y2="404.36" xlink:href="#gh"/>
        <linearGradient id="kg" x1="298.86" y1="399.77" x2="273.23"
                        y2="397.42" xlink:href="#gh"/>
        <linearGradient id="kh" x1="299.5" y1="392.73" x2="273.88"
                        y2="390.37" xlink:href="#gh"/>
        <linearGradient id="ki" x1="300.15" y1="385.69" x2="274.52"
                        y2="383.33" xlink:href="#gh"/>
        <linearGradient id="kj" x1="301.1" y1="375.37" x2="275.47"
                        y2="373.02" xlink:href="#gh"/>
        <linearGradient id="kk" x1="301.9" y1="366.65" x2="276.27"
                        y2="364.29" xlink:href="#gh"/>
        <linearGradient id="kl" x1="302.85" y1="356.23" x2="277.23"
                        y2="353.88" xlink:href="#gh"/>
        <linearGradient id="km" x1="303.65" y1="347.51" x2="278.03"
                        y2="345.16" xlink:href="#gh"/>
        <linearGradient id="kn" x1="304.61" y1="337.09" x2="278.98"
                        y2="334.74" xlink:href="#gh"/>
        <linearGradient id="ko" x1="305.41" y1="328.37" x2="279.78"
                        y2="326.02" xlink:href="#gh"/>
        <linearGradient id="kp" x1="306.36" y1="318.06" x2="280.73"
                        y2="315.7" xlink:href="#gh"/>
        <linearGradient id="kq" x1="307.49" y1="305.76" x2="281.86"
                        y2="303.41" xlink:href="#gh"/>
        <linearGradient id="kr" x1="308.29" y1="297.03" x2="282.66"
                        y2="294.68" xlink:href="#gh"/>
        <linearGradient id="ks" x1="309.23" y1="286.72" x2="283.61"
                        y2="284.37" xlink:href="#gh"/>
        <linearGradient id="kt" x1="383.15" y1="418.72" x2="357.47"
                        y2="416.7" xlink:href="#gh"/>
        <linearGradient id="ku" x1="383.96" y1="408.39" x2="358.28"
                        y2="406.37" xlink:href="#gh"/>
        <linearGradient id="kv" x1="384.65" y1="399.64" x2="358.97"
                        y2="397.62" xlink:href="#gh"/>
        <linearGradient id="kw" x1="385.47" y1="389.21" x2="359.79"
                        y2="387.19" xlink:href="#gh"/>
        <linearGradient id="kx" x1="386.43" y1="376.98" x2="360.75"
                        y2="374.96" xlink:href="#gh"/>
        <linearGradient id="ky" x1="387.12" y1="368.24" x2="361.44"
                        y2="366.22" xlink:href="#gh"/>
        <linearGradient id="kz" x1="387.94" y1="357.8" x2="362.26"
                        y2="355.78" xlink:href="#gh"/>
        <linearGradient id="la" x1="388.9" y1="345.58" x2="363.22"
                        y2="343.56" xlink:href="#gh"/>
        <linearGradient id="lb" x1="389.59" y1="336.83" x2="363.91"
                        y2="334.81" xlink:href="#gh"/>
        <linearGradient id="lc" x1="390.41" y1="326.39" x2="364.73"
                        y2="324.37" xlink:href="#gh"/>
        <linearGradient id="ld" x1="391.1" y1="317.65" x2="365.42"
                        y2="315.63" xlink:href="#gh"/>
        <linearGradient id="le" x1="391.92" y1="307.21" x2="366.24"
                        y2="305.19" xlink:href="#gh"/>
        <linearGradient id="lf" x1="392.88" y1="294.99" x2="367.2"
                        y2="292.97" xlink:href="#gh"/>
        <linearGradient id="lg" x1="393.84" y1="282.76" x2="368.16"
                        y2="280.74" xlink:href="#gh"/>
        <linearGradient id="lh" x1="394.53" y1="274.02" x2="368.85"
                        y2="272" xlink:href="#gh"/>
        <linearGradient id="li" x1="395.35" y1="263.58" x2="369.67"
                        y2="261.56" xlink:href="#gh"/>
        <linearGradient id="lj" x1="452" y1="642" x2="532" y2="642"
                        xlink:href="#a"/>
        <linearGradient id="lk" x1="412.12" y1="263.12" x2="491.88"
                        y2="182.88" xlink:href="#gh"/>
        <linearGradient id="ll" x1="344.12" y1="738.12" x2="423.88"
                        y2="657.88" xlink:href="#eo"/>
        <linearGradient id="lm" x1="24" x2="304" xlink:href="#a"/>
        <linearGradient id="ln" x1="27.01" y1="266.37" x2="298.51"
                        y2="622.37" gradientTransform="translate(0 -126)"
                        gradientUnits="userSpaceOnUse">
          <stop offset="0" stop-color="#734fcf" stop-opacity="0.77"/>
          <stop offset="1" stop-color="#493d8b" stop-opacity="0.36"/>
        </linearGradient>
        <linearGradient id="lo" x1="73.05" y1="587.55" x2="94.95"
                        y2="532.45" xlink:href="#eo"/>
        <linearGradient id="lp" x1="24" y1="281" x2="304" y2="281"
                        xlink:href="#a"/>
        <clipPath id="lq">
          <rect x="68" y="232" width="185.8" height="87.4" fill="none"/>
        </clipPath>
        <linearGradient id="lr" x1="70" y1="366" x2="228" y2="366"
                        xlink:href="#a"/>
        <linearGradient id="ls" x1="70" y1="390" x2="192" y2="390"
                        xlink:href="#a"/>
        <linearGradient id="lt" x1="70" y1="414" x2="228" y2="414"
                        xlink:href="#a"/>
        <linearGradient id="lu" x1="70" y1="438" x2="219" y2="438"
                        xlink:href="#a"/>
        <linearGradient id="lv" x1="70" y1="461.2" x2="228" y2="461.2"
                        xlink:href="#a"/>
        <linearGradient id="lw" x1="70" y1="485.2" x2="192" y2="485.2"
                        xlink:href="#a"/>
        <linearGradient id="lx" x1="70" y1="509.2" x2="228" y2="509.2"
                        xlink:href="#a"/>
        <linearGradient id="ly" x1="70" y1="533.2" x2="219" y2="533.2"
                        xlink:href="#a"/>
        <radialGradient id="lz" cx="-736.38" cy="-1.48" r="1"
                        gradientTransform="translate(-42601.14 333.05) rotate(180) scale(58.49 85.11)"
                        gradientUnits="userSpaceOnUse">
          <stop offset="0" stop-color="#bacedc"/>
          <stop offset="0.51" stop-color="#32bba9"/>
          <stop offset="1" stop-color="#1174bb"/>
        </radialGradient>
        <linearGradient id="ma" x1="495" y1="552.89" x2="502.9"
                        y2="523.08" xlink:href="#gh"/>
        <linearGradient id="mb" x1="495" y1="572.89" x2="502.9"
                        y2="543.08" xlink:href="#gh"/>
        <linearGradient id="mc" x1="493.54" y1="590.72" x2="503.33"
                        y2="566.81" xlink:href="#g"/>
        <radialGradient id="md" cx="-726.67" cy="-0.27" r="1"
                        gradientTransform="translate(-196849.26 358.11) rotate(180) scale(271.38 470.12)"
                        xlink:href="#lz"/>
        <linearGradient id="me" x1="894.87" y1="769.58" x2="1012.79"
                        y2="712.85"
                        gradientTransform="translate(-443.8 -452.96)"
                        gradientUnits="userSpaceOnUse">
          <stop offset="0" stop-color="#f7762e" stop-opacity="0"/>
          <stop offset="1" stop-color="#e67639"/>
        </linearGradient>
        <linearGradient id="mf" x1="939.09" y1="770.36" x2="950.51"
                        y2="718.25"
                        gradientTransform="translate(-443.8 -452.96)"
                        gradientUnits="userSpaceOnUse">
          <stop offset="0" stop-color="#a0cd37" stop-opacity="0.06"/>
          <stop offset="1" stop-color="#a0cd37"/>
        </linearGradient>
        <linearGradient id="mg" x1="992.19" y1="789.47" x2="893.67"
                        y2="723.25"
                        gradientTransform="translate(-443.8 -452.96)"
                        gradientUnits="userSpaceOnUse">
          <stop offset="0" stop-color="#32b2a3" stop-opacity="0.04"/>
          <stop offset="1" stop-color="#3ab0a4"/>
        </linearGradient>
      </defs>
      <g class="waves-wrapper">

        <circle class="big-base" cx="320" cy="320" r="309" opacity="0.04"
                fill="url(#a)" style="isolation:isolate"/>
        <circle class="large-wave wave" cx="320" cy="320" r="285"
                fill="none" stroke-width="2" opacity="0.4"
                stroke="url(#b)"
                style="isolation:isolate"/>
        <circle class="medium-wave wave" cx="320" cy="320" r="262"
                fill="none" stroke-width="2" opacity="0.6"
                stroke="url(#c)"
                style="isolation:isolate"/>
        <circle class="small-wave wave" cx="320" cy="320" r="235"
                fill="none" stroke-width="2" stroke="url(#d)"/>
        <circle class="small-base" cx="320" cy="320" r="220"
                opacity="0.08" fill="url(#e)" style="isolation:isolate"/>
      </g>
      <g class="clint-wrapper">
        <g class="clint clint-1">
          <circle cx="492" cy="516" r="40" fill="url(#lj)"/>
          <path
            d="M492.6,501.2a4.8,4.8,0,1,0,4.8,4.8A4.8,4.8,0,0,0,492.6,501.2Zm-9.6,4.7a9.6,9.6,0,1,1,9.6,9.6A9.6,9.6,0,0,1,483,505.9Zm4.2,12a12,12,0,0,0-11.9,12v4.8a2.4,2.4,0,0,0,4.8,0v-4.8a7.17,7.17,0,0,1,7.2-7.2h9.6a7.17,7.17,0,0,1,7.2,7.2v4.8a2.4,2.4,0,0,0,4.8,0v-4.8a12,12,0,0,0-11.9-12Z"
            fill="#d9d2e8" fill-rule="evenodd"/>
        </g>
        <g class="clint clint-2">
          <circle cx="452" cy="97" r="40" fill="url(#lk)"/>
          <path
            d="M452.6,82.2a4.8,4.8,0,1,0,4.8,4.8A4.8,4.8,0,0,0,452.6,82.2ZM443,86.9a9.6,9.6,0,1,1,9.6,9.6A9.6,9.6,0,0,1,443,86.9Zm4.2,12a12,12,0,0,0-11.9,12v4.8a2.4,2.4,0,0,0,4.8,0v-4.8a7.17,7.17,0,0,1,7.2-7.2h9.6a7.17,7.17,0,0,1,7.2,7.2v4.8a2.4,2.4,0,1,0,4.8,0v-4.8a12,12,0,0,0-11.9-12Z"
            fill="#dbe2cb" fill-rule="evenodd"/>
        </g>
        <g class="clint clint-3">
          <circle cx="384" cy="572" r="40" fill="url(#ll)"/>
          <path
            d="M384.6,557.2a4.8,4.8,0,1,0,4.8,4.8A4.8,4.8,0,0,0,384.6,557.2Zm-9.6,4.7a9.6,9.6,0,1,1,9.6,9.6A9.6,9.6,0,0,1,375,561.9Zm4.2,12a12,12,0,0,0-11.9,12v4.8a2.4,2.4,0,1,0,4.8,0v-4.8a7.17,7.17,0,0,1,7.2-7.2h9.6a7.17,7.17,0,0,1,7.2,7.2v4.8a2.4,2.4,0,0,0,4.8,0v-4.8a12,12,0,0,0-11.9-12Z"
            fill="#fee4d5" fill-rule="evenodd"/>
        </g>
      </g>
      <g class="small-circle-wrapper">
        <path
          d="M83,331.6a3.4,3.4,0,1,0-3.4-3.4A3.37,3.37,0,0,0,83,331.6Z"
          fill="#32bba9"/>
        <path
          d="M117.1,105.5a3.8,3.8,0,1,0-3.8-3.8A3.74,3.74,0,0,0,117.1,105.5Z"
          fill="#d4088c"/>
        <path
          d="M565.5,228.7a2,2,0,0,1-.4,1.3,2.84,2.84,0,0,1-1,.9,2,2,0,0,1-1.3.1,2.12,2.12,0,0,1-1.8-1.8,2.77,2.77,0,0,1,.1-1.3,2.19,2.19,0,0,1,.9-1,2,2,0,0,1,1.3-.4,2,2,0,0,1,1.6.7A2.29,2.29,0,0,1,565.5,228.7Z"
          fill="#fb762b"/>
        <path
          d="M168.2,410.7a3.45,3.45,0,0,1-.5,1.8,3.49,3.49,0,0,1-1.5,1.2,3.29,3.29,0,0,1-3.6-.7,2.84,2.84,0,0,1-.9-1.7,3.13,3.13,0,0,1,.2-1.9,4.43,4.43,0,0,1,1.2-1.5,3,3,0,0,1,1.8-.6,2.9,2.9,0,0,1,2.3,1A3.69,3.69,0,0,1,168.2,410.7Z"
          fill="#6ee8fc"/>
        <path
          d="M49.8,289.7a1.9,1.9,0,1,0-1.9-1.9A1.84,1.84,0,0,0,49.8,289.7Z"
          fill="#a0cd37"/>
        <path
          d="M396.5,50.3a2,2,0,0,1-.4,1.3,2.84,2.84,0,0,1-1,.9,2,2,0,0,1-1.3.1,2.12,2.12,0,0,1-1.8-1.8,2.77,2.77,0,0,1,.1-1.3,2.19,2.19,0,0,1,.9-1,2,2,0,0,1,1.3-.4,2,2,0,0,1,1.6.7A2.29,2.29,0,0,1,396.5,50.3Z"
          fill="#d4088c"/>
        <path d="M464,532a3.2,3.2,0,1,0-3.2-3.2A3.16,3.16,0,0,0,464,532Z"
              fill="#d4088c"/>
        <path d="M431.7,503a2.05,2.05,0,1,1-.6-1.4A2,2,0,0,1,431.7,503Z"
              fill="#d4088c"/>
        <path d="M469.6,494.4a1,1,0,1,1-.3-.7A.78.78,0,0,1,469.6,494.4Z"
              fill="#d4088c"/>
      </g>
      <g class="numbers" clip-path="url(#f)">
        <g opacity=".3" class="numbers-move">
          <g>
            <path
              d="M242,435.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,435.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,435.8Z"
              fill="url(#g)"/>
            <path d="M229.2,424.5h12.6v2.3H231.2v2.7h-2Z" fill="url(#h)"/>
            <path d="M229.2,417.5h12.6v2.3H231.2v2.7h-2Z" fill="url(#i)"/>
            <path d="M229.2,410.4h12.6v2.3H231.2v2.7h-2Z" fill="url(#j)"/>
            <path d="M229.2,403.3h12.6v2.3H231.2v2.7h-2Z" fill="url(#k)"/>
            <path
              d="M242,395.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,239,400a7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,395.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,395.4Z"
              fill="url(#l)"/>
            <path d="M229.2,384.1h12.6v2.3H231.2v2.7h-2Z" fill="url(#m)"/>
            <path
              d="M242,376.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,376.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,376.1Z"
              fill="url(#n)"/>
            <path d="M229.2,364.8h12.6v2.3H231.2v2.7h-2Z" fill="url(#o)"/>
            <path
              d="M242,356.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,356.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,356.8Z"
              fill="url(#p)"/>
            <path d="M229.2,345.5h12.6v2.3H231.2v2.7h-2Z" fill="url(#q)"/>
            <path
              d="M242,337.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,232,333a7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,242,337.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,240,337.6Z"
              fill="url(#r)"/>
            <path
              d="M242,325.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,325.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,325.2Z"
              fill="url(#s)"/>
            <path d="M229.2,313.9h12.6v2.3H231.2v2.7h-2Z" fill="url(#t)"/>
            <path
              d="M242,306a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,306Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,306Z"
              fill="url(#u)"/>
            <path d="M229.2,294.6h12.6v2.3H231.2v2.7h-2Z" fill="url(#v)"/>
            <path
              d="M242,286.7a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,286.7Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,286.7Z"
              fill="url(#w)"/>
            <path d="M229.2,275.4h12.6v2.3H231.2v2.7h-2Z" fill="url(#x)"/>
            <path
              d="M242,267.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,239,272a7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,267.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,267.4Z"
              fill="url(#y)"/>
            <path
              d="M242,255.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,255.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,255.1Z"
              fill="url(#z)"/>
            <path
              d="M242,242.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,242.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,242.8Z"
              fill="url(#aa)"/>
            <path d="M229.2,231.5h12.6v2.3H231.2v2.7h-2Z"
                  fill="url(#ab)"/>
            <path
              d="M242,223.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,223.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,223.5Z"
              fill="url(#ac)"/>
            <path d="M229.2,212.2h12.6v2.3H231.2v2.7h-2Z"
                  fill="url(#ad)"/>
            <path
              d="M242,204.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,204.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,204.2Z"
              fill="url(#ae)"/>
            <path d="M229.2,192.9h12.6v2.3H231.2v2.7h-2Z"
                  fill="url(#af)"/>
            <path
              d="M325,435.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,435.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,435.8Z"
              fill="url(#ag)"/>
            <path d="M312.2,424.5h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path d="M312.2,417.5h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path d="M312.2,410.4h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path d="M312.2,403.3h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,395.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,322,400a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,395.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,395.4Z"
              fill="url(#ah)"/>
            <path d="M312.2,384.1h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,376.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,376.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,376.1Z"
              fill="url(#ah)"/>
            <path d="M312.2,364.8h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,356.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,356.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,356.8Z"
              fill="url(#ah)"/>
            <path d="M312.2,345.5h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,337.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,315,333a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,325,337.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,323,337.6Z"
              fill="url(#ah)"/>
            <path
              d="M325,325.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,325.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,325.2Z"
              fill="url(#ah)"/>
            <path d="M312.2,313.9h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,306a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,306Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,306Z"
              fill="url(#ah)"/>
            <path d="M312.2,294.6h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,286.7a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,286.7Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,286.7Z"
              fill="url(#ah)"/>
            <path d="M312.2,275.4h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,267.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,322,272a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,267.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,267.4Z"
              fill="url(#ah)"/>
            <path
              d="M325,255.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,255.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,255.1Z"
              fill="url(#ah)"/>
            <path
              d="M325,242.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,242.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,242.8Z"
              fill="url(#ah)"/>
            <path d="M312.2,231.5h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,223.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,223.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,223.5Z"
              fill="url(#ah)"/>
            <path d="M312.2,212.2h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,204.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,204.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,204.2Z"
              fill="url(#ah)"/>
            <path d="M312.2,192.9h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M442,435.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,435.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,435.8Z"
              fill="url(#bg)"/>
            <path d="M429.2,424.5h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path d="M429.2,417.5h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path d="M429.2,410.4h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path d="M429.2,403.3h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,395.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,439,400a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,395.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,395.4Z"
              fill="url(#bh)"/>
            <path d="M429.2,384.1h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,376.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,376.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,376.1Z"
              fill="url(#bh)"/>
            <path d="M429.2,364.8h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,356.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,356.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,356.8Z"
              fill="url(#bh)"/>
            <path d="M429.2,345.5h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,337.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,432,333a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,442,337.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,440,337.6Z"
              fill="url(#bh)"/>
            <path
              d="M442,325.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,325.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,325.2Z"
              fill="url(#bh)"/>
            <path d="M429.2,313.9h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,306a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,306Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,306Z"
              fill="url(#bh)"/>
            <path d="M429.2,294.6h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,286.7a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,286.7Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,286.7Z"
              fill="url(#bh)"/>
            <path d="M429.2,275.4h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,267.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,439,272a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,267.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,267.4Z"
              fill="url(#bh)"/>
            <path
              d="M442,255.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,255.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,255.1Z"
              fill="url(#bh)"/>
            <path
              d="M442,242.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,242.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,242.8Z"
              fill="url(#bh)"/>
            <path d="M429.2,231.5h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,223.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,223.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,223.5Z"
              fill="url(#bh)"/>
            <path d="M429.2,212.2h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,204.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,204.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,204.2Z"
              fill="url(#bh)"/>
            <path d="M429.2,192.9h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M269,502.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,502.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,502.8Z"
              fill="url(#cg)"/>
            <path d="M256.2,491.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#ch)"/>
            <path d="M256.2,484.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#ci)"/>
            <path d="M256.2,477.4h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#cj)"/>
            <path d="M256.2,470.3h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#ck)"/>
            <path
              d="M269,462.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,266,467a7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,462.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,462.4Z"
              fill="url(#cl)"/>
            <path d="M256.2,451.1h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#cm)"/>
            <path
              d="M269,443.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,443.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,443.1Z"
              fill="url(#cn)"/>
            <path d="M256.2,431.8h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#co)"/>
            <path
              d="M269,423.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,423.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,423.8Z"
              fill="url(#cp)"/>
            <path d="M256.2,412.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#cq)"/>
            <path
              d="M269,404.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,259,400a7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,269,404.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,267,404.6Z"
              fill="url(#cr)"/>
            <path
              d="M269,392.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,392.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,392.2Z"
              fill="url(#cs)"/>
            <path d="M256.2,380.9h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#ct)"/>
            <path
              d="M269,373a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,373Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,373Z"
              fill="url(#cu)"/>
            <path
              d="M355,502.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,502.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,502.8Z"
              fill="url(#cv)"/>
            <path d="M342.2,491.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#cw)"/>
            <path d="M342.2,484.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#cx)"/>
            <path d="M342.2,477.4h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#cy)"/>
            <path d="M342.2,470.3h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#cz)"/>
            <path
              d="M355,462.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,352,467a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,462.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,462.4Z"
              fill="url(#da)"/>
            <path d="M342.2,451.1h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#db)"/>
            <path
              d="M355,443.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,443.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,443.1Z"
              fill="url(#dc)"/>
            <path d="M342.2,431.8h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#dd)"/>
            <path
              d="M355,423.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,423.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,423.8Z"
              fill="url(#de)"/>
            <path d="M342.2,412.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#df)"/>
            <path
              d="M355,404.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,345,400a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,355,404.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,353,404.6Z"
              fill="url(#dg)"/>
            <path
              d="M355,392.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,392.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,392.2Z"
              fill="url(#dh)"/>
            <path d="M342.2,380.9h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#di)"/>
            <path
              d="M355,373a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,373Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,373Z"
              fill="url(#dj)"/>
            <path
              d="M413,299.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,299.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,299.8Z"
              fill="url(#dk)"/>
            <path d="M400.2,288.5h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#dl)"/>
            <path d="M400.2,281.5h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#dm)"/>
            <path d="M400.2,274.4h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#dn)"/>
            <path d="M400.2,267.3h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#do)"/>
            <path
              d="M413,259.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,410,264a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,259.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,259.4Z"
              fill="url(#dp)"/>
            <path d="M400.2,248.1h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#dq)"/>
            <path
              d="M413,240.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,240.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,240.1Z"
              fill="url(#dr)"/>
            <path d="M400.2,228.8h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#ds)"/>
            <path
              d="M413,220.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,220.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,220.8Z"
              fill="url(#dt)"/>
            <path d="M400.2,209.5h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#du)"/>
            <path
              d="M413,201.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,403,197a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,413,201.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,411,201.6Z"
              fill="url(#dv)"/>
            <path
              d="M413,189.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,189.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,189.2Z"
              fill="url(#dw)"/>
            <path d="M400.2,177.9h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#dx)"/>
            <path
              d="M413,170a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,170Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,170Z"
              fill="url(#dy)"/>
            <path
              d="M472,502.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,502.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,502.8Z"
              fill="url(#dz)"/>
            <path d="M459.2,491.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ea)"/>
            <path d="M459.2,484.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#eb)"/>
            <path d="M459.2,477.4h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ec)"/>
            <path d="M459.2,470.3h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ed)"/>
            <path
              d="M472,462.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,469,467a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,462.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,462.4Z"
              fill="url(#ee)"/>
            <path d="M459.2,451.1h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ef)"/>
            <path
              d="M472,443.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,443.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,443.1Z"
              fill="url(#eg)"/>
            <path d="M459.2,431.8h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#eh)"/>
            <path
              d="M472,423.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,423.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,423.8Z"
              fill="url(#ei)"/>
            <path d="M459.2,412.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ej)"/>
            <path
              d="M472,404.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,462,400a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,472,404.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,470,404.6Z"
              fill="url(#ek)"/>
            <path
              d="M472,392.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,392.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,392.2Z"
              fill="url(#el)"/>
            <path d="M459.2,380.9h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#em)"/>
            <path
              d="M472,373a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,373Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,373Z"
              fill="url(#en)"/>
            <path
              d="M297,446.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,446.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,446.8Z"
              fill="url(#eo)"/>
            <path d="M284.2,435.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#ep)"/>
            <path d="M284.2,428.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#eq)"/>
            <path d="M284.2,421.4h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#er)"/>
            <path d="M284.2,414.3h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#es)"/>
            <path
              d="M297,406.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,294,411a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,406.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,406.4Z"
              fill="url(#et)"/>
            <path d="M284.2,395.1h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#eu)"/>
            <path
              d="M297,387.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,387.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,387.1Z"
              fill="url(#ev)"/>
            <path d="M284.2,375.8h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#ew)"/>
            <path
              d="M297,367.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,367.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,367.8Z"
              fill="url(#ex)"/>
            <path d="M284.2,356.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#ey)"/>
            <path
              d="M297,348.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,287,344a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,297,348.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,295,348.6Z"
              fill="url(#ez)"/>
            <path
              d="M297,336.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,336.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,336.2Z"
              fill="url(#fa)"/>
            <path d="M284.2,324.9h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#fb)"/>
            <path
              d="M297,317a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,317Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,317Z"
              fill="url(#fc)"/>
            <path
              d="M414,446.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,446.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,446.8Z"
              fill="url(#fd)"/>
            <path d="M401.2,435.5h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fe)"/>
            <path d="M401.2,428.5h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#ff)"/>
            <path d="M401.2,421.4h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fg)"/>
            <path d="M401.2,414.3h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fh)"/>
            <path
              d="M414,406.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,411,411a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,406.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,406.4Z"
              fill="url(#fi)"/>
            <path d="M401.2,395.1h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fj)"/>
            <path
              d="M414,387.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,387.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,387.1Z"
              fill="url(#fk)"/>
            <path d="M401.2,375.8h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fl)"/>
            <path
              d="M414,367.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,367.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,367.8Z"
              fill="url(#fm)"/>
            <path d="M401.2,356.5h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fn)"/>
            <path
              d="M414,348.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,404,344a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,414,348.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,412,348.6Z"
              fill="url(#fo)"/>
            <path
              d="M414,336.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,336.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,336.2Z"
              fill="url(#fp)"/>
            <path d="M401.2,324.9h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fq)"/>
            <path
              d="M414,317a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,317Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,317Z"
              fill="url(#fr)"/>
            <path d="M370.2,447.7h12.6V450H372.2v2.7h-2Z"
                  fill="url(#fs)"/>
            <path d="M370.2,440.7h12.6V443H372.2v2.7h-2Z"
                  fill="url(#ft)"/>
            <path d="M370.2,433.6h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#fu)"/>
            <path d="M370.2,426.6h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#fv)"/>
            <path
              d="M383,418.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,373,414a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,418.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,418.6Z"
              fill="url(#fw)"/>
            <path d="M370.2,407.3h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#fx)"/>
            <path
              d="M383,399.3a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,399.3Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,399.3Z"
              fill="url(#fy)"/>
            <path d="M370.2,388h12.6v2.3H372.2V393h-2Z" fill="url(#fz)"/>
            <path
              d="M383,380.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,383,380.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,381,380.1Z"
              fill="url(#ga)"/>
            <path d="M370.2,368.7h12.6V371H372.2v2.7h-2Z"
                  fill="url(#gb)"/>
            <path
              d="M383,360.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,360.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,360.8Z"
              fill="url(#gc)"/>
            <path
              d="M383,348.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,348.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,348.5Z"
              fill="url(#gd)"/>
            <path d="M370.2,337.1h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#ge)"/>
            <path
              d="M383,329.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,329.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,329.2Z"
              fill="url(#gf)"/>
            <path
              d="M383,316.9a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,316.9Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,316.9Z"
              fill="url(#gg)"/>
            <path
              d="M269,344.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,344.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,344.8Z"
              fill="url(#gh)"/>
            <path d="M256.2,333.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gi)"/>
            <path d="M256.2,326.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gj)"/>
            <path d="M256.2,319.4h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gk)"/>
            <path d="M256.2,312.3h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gl)"/>
            <path
              d="M269,304.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,266,309a7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,304.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,304.4Z"
              fill="url(#gm)"/>
            <path d="M256.2,293.1h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gn)"/>
            <path
              d="M269,285.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,285.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,285.1Z"
              fill="url(#go)"/>
            <path d="M256.2,273.8h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gp)"/>
            <path
              d="M269,265.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,265.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,265.8Z"
              fill="url(#gq)"/>
            <path d="M256.2,254.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gr)"/>
            <path
              d="M269,246.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,259,242a7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,269,246.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,267,246.6Z"
              fill="url(#gs)"/>
            <path
              d="M269,234.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,234.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,234.2Z"
              fill="url(#gt)"/>
            <path d="M256.2,222.9h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gu)"/>
            <path
              d="M269,215a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,215Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,215Z"
              fill="url(#gv)"/>
            <path
              d="M355,344.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,344.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,344.8Z"
              fill="url(#gw)"/>
            <path d="M342.2,333.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#gx)"/>
            <path d="M342.2,326.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#gy)"/>
            <path d="M342.2,319.4h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#gz)"/>
            <path d="M342.2,312.3h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#ha)"/>
            <path
              d="M355,304.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,352,309a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,304.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,304.4Z"
              fill="url(#hb)"/>
            <path d="M342.2,293.1h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#hc)"/>
            <path
              d="M355,285.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,285.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,285.1Z"
              fill="url(#hd)"/>
            <path d="M342.2,273.8h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#he)"/>
            <path
              d="M355,265.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,265.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,265.8Z"
              fill="url(#hf)"/>
            <path d="M342.2,254.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#hg)"/>
            <path
              d="M355,246.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,345,242a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,355,246.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,353,246.6Z"
              fill="url(#hh)"/>
            <path
              d="M355,234.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,234.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,234.2Z"
              fill="url(#hi)"/>
            <path d="M342.2,222.9h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#hj)"/>
            <path
              d="M355,215a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,215Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,215Z"
              fill="url(#hk)"/>
            <path
              d="M472,344.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,344.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,344.8Z"
              fill="url(#hl)"/>
            <path d="M459.2,333.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hm)"/>
            <path d="M459.2,326.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hn)"/>
            <path d="M459.2,319.4h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ho)"/>
            <path d="M459.2,312.3h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hp)"/>
            <path
              d="M472,304.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,469,309a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,304.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,304.4Z"
              fill="url(#hq)"/>
            <path d="M459.2,293.1h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hr)"/>
            <path
              d="M472,285.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,285.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,285.1Z"
              fill="url(#hs)"/>
            <path d="M459.2,273.8h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ht)"/>
            <path
              d="M472,265.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,265.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,265.8Z"
              fill="url(#hu)"/>
            <path d="M459.2,254.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hv)"/>
            <path
              d="M472,246.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,462,242a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,472,246.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,470,246.6Z"
              fill="url(#hw)"/>
            <path
              d="M472,234.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,234.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,234.2Z"
              fill="url(#hx)"/>
            <path d="M459.2,222.9h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hy)"/>
            <path
              d="M472,215a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,215Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,215Z"
              fill="url(#hz)"/>
            <path
              d="M176.4,435.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,435.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,435.8Z"
              fill="url(#ia)"/>
            <path d="M163.6,424.5h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path d="M163.6,417.5h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path d="M163.6,410.4h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path d="M163.6,403.3h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,395.4a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,395.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,395.4Z"
              fill="url(#ib)"/>
            <path d="M163.6,384.1h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,376.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,376.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,376.1Z"
              fill="url(#ib)"/>
            <path d="M163.6,364.8h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,356.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,356.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,356.8Z"
              fill="url(#ib)"/>
            <path d="M163.6,345.5h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,337.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,176.4,337.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,174.4,337.6Z"
              fill="url(#ib)"/>
            <path
              d="M176.4,325.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,325.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,325.2Z"
              fill="url(#ib)"/>
            <path d="M163.6,313.9h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,306a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,306Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,306Z"
              fill="url(#ib)"/>
            <path d="M163.6,294.6h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,286.7a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,286.7Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,286.7Z"
              fill="url(#ib)"/>
            <path d="M163.6,275.4h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,267.4a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,267.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,267.4Z"
              fill="url(#ib)"/>
            <path
              d="M176.4,255.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,255.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,255.1Z"
              fill="url(#ib)"/>
            <path
              d="M176.4,242.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,242.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,242.8Z"
              fill="url(#ib)"/>
            <path d="M163.6,231.5h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,223.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,223.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,223.5Z"
              fill="url(#ib)"/>
            <path d="M163.6,212.2h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,204.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,204.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,204.2Z"
              fill="url(#ib)"/>
            <path d="M163.6,192.9h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M206.4,502.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,502.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,502.8Z"
              fill="url(#ja)"/>
            <path d="M193.6,491.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jb)"/>
            <path d="M193.6,484.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jc)"/>
            <path d="M193.6,477.4h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jd)"/>
            <path d="M193.6,470.3h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#je)"/>
            <path
              d="M206.4,462.4a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,462.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,462.4Z"
              fill="url(#jf)"/>
            <path d="M193.6,451.1h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jg)"/>
            <path
              d="M206.4,443.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,443.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,443.1Z"
              fill="url(#jh)"/>
            <path d="M193.6,431.8h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#ji)"/>
            <path
              d="M206.4,423.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,423.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,423.8Z"
              fill="url(#jj)"/>
            <path d="M193.6,412.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jk)"/>
            <path
              d="M206.4,404.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,206.4,404.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,204.4,404.6Z"
              fill="url(#jl)"/>
            <path
              d="M206.4,392.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,392.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,392.2Z"
              fill="url(#jm)"/>
            <path d="M193.6,380.9h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jn)"/>
            <path
              d="M206.4,373a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,373Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,373Z"
              fill="url(#jo)"/>
            <path
              d="M206.4,344.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,344.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,344.8Z"
              fill="url(#jp)"/>
            <path d="M193.6,333.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jq)"/>
            <path d="M193.6,326.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jr)"/>
            <path d="M193.6,319.4h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#js)"/>
            <path d="M193.6,312.3h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jt)"/>
            <path
              d="M206.4,304.4a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,304.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,304.4Z"
              fill="url(#ju)"/>
            <path d="M193.6,293.1h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jv)"/>
            <path
              d="M206.4,285.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,285.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,285.1Z"
              fill="url(#jw)"/>
            <path d="M193.6,273.8h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jx)"/>
            <path
              d="M206.4,265.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,265.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,265.8Z"
              fill="url(#jy)"/>
            <path d="M193.6,254.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jz)"/>
            <path
              d="M206.4,246.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,206.4,246.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,204.4,246.6Z"
              fill="url(#ka)"/>
            <path
              d="M206.4,234.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,234.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,234.2Z"
              fill="url(#kb)"/>
            <path d="M193.6,222.9h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#kc)"/>
            <path
              d="M206.4,215a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,215Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,215Z"
              fill="url(#kd)"/>
            <path
              d="M297,288.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,288.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,288.8Z"
              fill="url(#ke)"/>
            <path d="M284.2,277.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#kf)"/>
            <path d="M284.2,270.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#kg)"/>
            <path d="M284.2,263.4h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#kh)"/>
            <path d="M284.2,256.3h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#ki)"/>
            <path
              d="M297,248.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,294,253a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,248.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,248.4Z"
              fill="url(#kj)"/>
            <path d="M284.2,237.1h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#kk)"/>
            <path
              d="M297,229.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,229.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,229.1Z"
              fill="url(#kl)"/>
            <path d="M284.2,217.8h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#km)"/>
            <path
              d="M297,209.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,209.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,209.8Z"
              fill="url(#kn)"/>
            <path d="M284.2,198.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#ko)"/>
            <path
              d="M297,190.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,287,186a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,297,190.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,295,190.6Z"
              fill="url(#kp)"/>
            <path
              d="M297,178.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,178.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,178.2Z"
              fill="url(#kq)"/>
            <path d="M284.2,166.9h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#kr)"/>
            <path
              d="M297,159a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,159Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,159Z"
              fill="url(#ks)"/>
            <path d="M370.2,289.7h12.6V292H372.2v2.7h-2Z"
                  fill="url(#kt)"/>
            <path
              d="M383,281.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,281.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,281.8Z"
              fill="url(#ku)"/>
            <path d="M370.2,270.5h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#kv)"/>
            <path
              d="M383,262.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,262.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,262.5Z"
              fill="url(#kw)"/>
            <path
              d="M383,250.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,250.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,250.2Z"
              fill="url(#kx)"/>
            <path d="M370.2,238.9h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#ky)"/>
            <path
              d="M383,230.9a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,230.9Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,230.9Z"
              fill="url(#kz)"/>
            <path
              d="M383,218.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,373,214a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,218.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,218.6Z"
              fill="url(#la)"/>
            <path d="M370.2,207.3h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#lb)"/>
            <path
              d="M383,199.3a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,199.3Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,199.3Z"
              fill="url(#lc)"/>
            <path d="M370.2,188h12.6v2.3H372.2V193h-2Z" fill="url(#ld)"/>
            <path
              d="M383,180a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,180Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,180Z"
              fill="url(#le)"/>
            <path
              d="M383,167.7a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,167.7Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,167.7Z"
              fill="url(#lf)"/>
            <path
              d="M383,155.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,380,160a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,155.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,155.4Z"
              fill="url(#lg)"/>
            <path d="M370.2,144.1h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#lh)"/>
            <path
              d="M383,136.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,136.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,136.1Z"
              fill="url(#li)"/>
          </g>
          <g>
            <path
              d="M242,435.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,435.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,435.8Z"
              fill="url(#g)"/>
            <path d="M229.2,424.5h12.6v2.3H231.2v2.7h-2Z" fill="url(#h)"/>
            <path d="M229.2,417.5h12.6v2.3H231.2v2.7h-2Z" fill="url(#i)"/>
            <path d="M229.2,410.4h12.6v2.3H231.2v2.7h-2Z" fill="url(#j)"/>
            <path d="M229.2,403.3h12.6v2.3H231.2v2.7h-2Z" fill="url(#k)"/>
            <path
              d="M242,395.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,239,400a7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,395.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,395.4Z"
              fill="url(#l)"/>
            <path d="M229.2,384.1h12.6v2.3H231.2v2.7h-2Z" fill="url(#m)"/>
            <path
              d="M242,376.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,376.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,376.1Z"
              fill="url(#n)"/>
            <path d="M229.2,364.8h12.6v2.3H231.2v2.7h-2Z" fill="url(#o)"/>
            <path
              d="M242,356.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,356.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,356.8Z"
              fill="url(#p)"/>
            <path d="M229.2,345.5h12.6v2.3H231.2v2.7h-2Z" fill="url(#q)"/>
            <path
              d="M242,337.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,232,333a7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,242,337.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,240,337.6Z"
              fill="url(#r)"/>
            <path
              d="M242,325.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,325.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,325.2Z"
              fill="url(#s)"/>
            <path d="M229.2,313.9h12.6v2.3H231.2v2.7h-2Z" fill="url(#t)"/>
            <path
              d="M242,306a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,306Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,306Z"
              fill="url(#u)"/>
            <path d="M229.2,294.6h12.6v2.3H231.2v2.7h-2Z" fill="url(#v)"/>
            <path
              d="M242,286.7a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,286.7Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,286.7Z"
              fill="url(#w)"/>
            <path d="M229.2,275.4h12.6v2.3H231.2v2.7h-2Z" fill="url(#x)"/>
            <path
              d="M242,267.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,239,272a7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,267.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,267.4Z"
              fill="url(#y)"/>
            <path
              d="M242,255.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,255.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,255.1Z"
              fill="url(#z)"/>
            <path
              d="M242,242.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,242.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,242.8Z"
              fill="url(#aa)"/>
            <path d="M229.2,231.5h12.6v2.3H231.2v2.7h-2Z"
                  fill="url(#ab)"/>
            <path
              d="M242,223.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,223.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,223.5Z"
              fill="url(#ac)"/>
            <path d="M229.2,212.2h12.6v2.3H231.2v2.7h-2Z"
                  fill="url(#ad)"/>
            <path
              d="M242,204.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,242,204.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,240,204.2Z"
              fill="url(#ae)"/>
            <path d="M229.2,192.9h12.6v2.3H231.2v2.7h-2Z"
                  fill="url(#af)"/>
            <path
              d="M325,435.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,435.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,435.8Z"
              fill="url(#ag)"/>
            <path d="M312.2,424.5h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path d="M312.2,417.5h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path d="M312.2,410.4h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path d="M312.2,403.3h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,395.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,322,400a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,395.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,395.4Z"
              fill="url(#ah)"/>
            <path d="M312.2,384.1h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,376.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,376.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,376.1Z"
              fill="url(#ah)"/>
            <path d="M312.2,364.8h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,356.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,356.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,356.8Z"
              fill="url(#ah)"/>
            <path d="M312.2,345.5h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,337.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,315,333a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,325,337.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,323,337.6Z"
              fill="url(#ah)"/>
            <path
              d="M325,325.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,325.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,325.2Z"
              fill="url(#ah)"/>
            <path d="M312.2,313.9h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,306a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,306Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,306Z"
              fill="url(#ah)"/>
            <path d="M312.2,294.6h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,286.7a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,286.7Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,286.7Z"
              fill="url(#ah)"/>
            <path d="M312.2,275.4h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,267.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,322,272a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,267.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,267.4Z"
              fill="url(#ah)"/>
            <path
              d="M325,255.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,255.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,255.1Z"
              fill="url(#ah)"/>
            <path
              d="M325,242.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,242.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,242.8Z"
              fill="url(#ah)"/>
            <path d="M312.2,231.5h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,223.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,223.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,223.5Z"
              fill="url(#ah)"/>
            <path d="M312.2,212.2h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M325,204.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,325,204.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,323,204.2Z"
              fill="url(#ah)"/>
            <path d="M312.2,192.9h12.6v2.3H314.2v2.7h-2Z"
                  fill="url(#ah)"/>
            <path
              d="M442,435.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,435.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,435.8Z"
              fill="url(#bg)"/>
            <path d="M429.2,424.5h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path d="M429.2,417.5h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path d="M429.2,410.4h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path d="M429.2,403.3h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,395.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,439,400a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,395.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,395.4Z"
              fill="url(#bh)"/>
            <path d="M429.2,384.1h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,376.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,376.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,376.1Z"
              fill="url(#bh)"/>
            <path d="M429.2,364.8h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,356.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,356.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,356.8Z"
              fill="url(#bh)"/>
            <path d="M429.2,345.5h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,337.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,432,333a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,442,337.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,440,337.6Z"
              fill="url(#bh)"/>
            <path
              d="M442,325.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,325.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,325.2Z"
              fill="url(#bh)"/>
            <path d="M429.2,313.9h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,306a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,306Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,306Z"
              fill="url(#bh)"/>
            <path d="M429.2,294.6h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,286.7a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,286.7Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,286.7Z"
              fill="url(#bh)"/>
            <path d="M429.2,275.4h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,267.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,439,272a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,267.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,267.4Z"
              fill="url(#bh)"/>
            <path
              d="M442,255.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,255.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,255.1Z"
              fill="url(#bh)"/>
            <path
              d="M442,242.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,242.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,242.8Z"
              fill="url(#bh)"/>
            <path d="M429.2,231.5h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,223.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,223.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,223.5Z"
              fill="url(#bh)"/>
            <path d="M429.2,212.2h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M442,204.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,442,204.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,440,204.2Z"
              fill="url(#bh)"/>
            <path d="M429.2,192.9h12.6v2.3H431.2v2.7h-2Z"
                  fill="url(#bh)"/>
            <path
              d="M269,502.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,502.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,502.8Z"
              fill="url(#cg)"/>
            <path d="M256.2,491.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#ch)"/>
            <path d="M256.2,484.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#ci)"/>
            <path d="M256.2,477.4h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#cj)"/>
            <path d="M256.2,470.3h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#ck)"/>
            <path
              d="M269,462.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,266,467a7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,462.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,462.4Z"
              fill="url(#cl)"/>
            <path d="M256.2,451.1h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#cm)"/>
            <path
              d="M269,443.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,443.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,443.1Z"
              fill="url(#cn)"/>
            <path d="M256.2,431.8h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#co)"/>
            <path
              d="M269,423.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,423.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,423.8Z"
              fill="url(#cp)"/>
            <path d="M256.2,412.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#cq)"/>
            <path
              d="M269,404.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,259,400a7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,269,404.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,267,404.6Z"
              fill="url(#cr)"/>
            <path
              d="M269,392.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,392.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,392.2Z"
              fill="url(#cs)"/>
            <path d="M256.2,380.9h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#ct)"/>
            <path
              d="M269,373a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,373Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,373Z"
              fill="url(#cu)"/>
            <path
              d="M355,502.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,502.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,502.8Z"
              fill="url(#cv)"/>
            <path d="M342.2,491.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#cw)"/>
            <path d="M342.2,484.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#cx)"/>
            <path d="M342.2,477.4h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#cy)"/>
            <path d="M342.2,470.3h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#cz)"/>
            <path
              d="M355,462.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,352,467a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,462.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,462.4Z"
              fill="url(#da)"/>
            <path d="M342.2,451.1h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#db)"/>
            <path
              d="M355,443.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,443.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,443.1Z"
              fill="url(#dc)"/>
            <path d="M342.2,431.8h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#dd)"/>
            <path
              d="M355,423.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,423.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,423.8Z"
              fill="url(#de)"/>
            <path d="M342.2,412.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#df)"/>
            <path
              d="M355,404.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,345,400a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,355,404.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,353,404.6Z"
              fill="url(#dg)"/>
            <path
              d="M355,392.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,392.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,392.2Z"
              fill="url(#dh)"/>
            <path d="M342.2,380.9h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#di)"/>
            <path
              d="M355,373a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,373Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,373Z"
              fill="url(#dj)"/>
            <path
              d="M413,299.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,299.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,299.8Z"
              fill="url(#dk)"/>
            <path d="M400.2,288.5h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#dl)"/>
            <path d="M400.2,281.5h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#dm)"/>
            <path d="M400.2,274.4h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#dn)"/>
            <path d="M400.2,267.3h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#do)"/>
            <path
              d="M413,259.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,410,264a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,259.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,259.4Z"
              fill="url(#dp)"/>
            <path d="M400.2,248.1h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#dq)"/>
            <path
              d="M413,240.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,240.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,240.1Z"
              fill="url(#dr)"/>
            <path d="M400.2,228.8h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#ds)"/>
            <path
              d="M413,220.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,220.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,220.8Z"
              fill="url(#dt)"/>
            <path d="M400.2,209.5h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#du)"/>
            <path
              d="M413,201.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,403,197a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,413,201.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,411,201.6Z"
              fill="url(#dv)"/>
            <path
              d="M413,189.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,189.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,189.2Z"
              fill="url(#dw)"/>
            <path d="M400.2,177.9h12.6v2.3H402.2v2.7h-2Z"
                  fill="url(#dx)"/>
            <path
              d="M413,170a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,413,170Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,411,170Z"
              fill="url(#dy)"/>
            <path
              d="M472,502.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,502.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,502.8Z"
              fill="url(#dz)"/>
            <path d="M459.2,491.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ea)"/>
            <path d="M459.2,484.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#eb)"/>
            <path d="M459.2,477.4h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ec)"/>
            <path d="M459.2,470.3h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ed)"/>
            <path
              d="M472,462.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,469,467a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,462.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,462.4Z"
              fill="url(#ee)"/>
            <path d="M459.2,451.1h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ef)"/>
            <path
              d="M472,443.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,443.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,443.1Z"
              fill="url(#eg)"/>
            <path d="M459.2,431.8h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#eh)"/>
            <path
              d="M472,423.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,423.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,423.8Z"
              fill="url(#ei)"/>
            <path d="M459.2,412.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ej)"/>
            <path
              d="M472,404.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,462,400a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,472,404.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,470,404.6Z"
              fill="url(#ek)"/>
            <path
              d="M472,392.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,392.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,392.2Z"
              fill="url(#el)"/>
            <path d="M459.2,380.9h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#em)"/>
            <path
              d="M472,373a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,373Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,373Z"
              fill="url(#en)"/>
            <path
              d="M297,446.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,446.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,446.8Z"
              fill="url(#eo)"/>
            <path d="M284.2,435.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#ep)"/>
            <path d="M284.2,428.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#eq)"/>
            <path d="M284.2,421.4h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#er)"/>
            <path d="M284.2,414.3h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#es)"/>
            <path
              d="M297,406.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,294,411a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,406.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,406.4Z"
              fill="url(#et)"/>
            <path d="M284.2,395.1h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#eu)"/>
            <path
              d="M297,387.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,387.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,387.1Z"
              fill="url(#ev)"/>
            <path d="M284.2,375.8h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#ew)"/>
            <path
              d="M297,367.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,367.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,367.8Z"
              fill="url(#ex)"/>
            <path d="M284.2,356.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#ey)"/>
            <path
              d="M297,348.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,287,344a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,297,348.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,295,348.6Z"
              fill="url(#ez)"/>
            <path
              d="M297,336.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,336.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,336.2Z"
              fill="url(#fa)"/>
            <path d="M284.2,324.9h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#fb)"/>
            <path
              d="M297,317a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,317Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,317Z"
              fill="url(#fc)"/>
            <path
              d="M414,446.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,446.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,446.8Z"
              fill="url(#fd)"/>
            <path d="M401.2,435.5h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fe)"/>
            <path d="M401.2,428.5h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#ff)"/>
            <path d="M401.2,421.4h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fg)"/>
            <path d="M401.2,414.3h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fh)"/>
            <path
              d="M414,406.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,411,411a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,406.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,406.4Z"
              fill="url(#fi)"/>
            <path d="M401.2,395.1h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fj)"/>
            <path
              d="M414,387.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,387.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,387.1Z"
              fill="url(#fk)"/>
            <path d="M401.2,375.8h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fl)"/>
            <path
              d="M414,367.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,367.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,367.8Z"
              fill="url(#fm)"/>
            <path d="M401.2,356.5h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fn)"/>
            <path
              d="M414,348.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,404,344a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,414,348.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,412,348.6Z"
              fill="url(#fo)"/>
            <path
              d="M414,336.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,336.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,336.2Z"
              fill="url(#fp)"/>
            <path d="M401.2,324.9h12.6v2.3H403.2v2.7h-2Z"
                  fill="url(#fq)"/>
            <path
              d="M414,317a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,414,317Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,412,317Z"
              fill="url(#fr)"/>
            <path d="M370.2,447.7h12.6V450H372.2v2.7h-2Z"
                  fill="url(#fs)"/>
            <path d="M370.2,440.7h12.6V443H372.2v2.7h-2Z"
                  fill="url(#ft)"/>
            <path d="M370.2,433.6h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#fu)"/>
            <path d="M370.2,426.6h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#fv)"/>
            <path
              d="M383,418.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,373,414a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,418.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,418.6Z"
              fill="url(#fw)"/>
            <path d="M370.2,407.3h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#fx)"/>
            <path
              d="M383,399.3a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,399.3Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,399.3Z"
              fill="url(#fy)"/>
            <path d="M370.2,388h12.6v2.3H372.2V393h-2Z" fill="url(#fz)"/>
            <path
              d="M383,380.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,383,380.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,381,380.1Z"
              fill="url(#ga)"/>
            <path d="M370.2,368.7h12.6V371H372.2v2.7h-2Z"
                  fill="url(#gb)"/>
            <path
              d="M383,360.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,360.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,360.8Z"
              fill="url(#gc)"/>
            <path
              d="M383,348.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,348.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,348.5Z"
              fill="url(#gd)"/>
            <path d="M370.2,337.1h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#ge)"/>
            <path
              d="M383,329.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,329.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,329.2Z"
              fill="url(#gf)"/>
            <path
              d="M383,316.9a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,316.9Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,316.9Z"
              fill="url(#gg)"/>
            <path
              d="M269,344.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,344.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,344.8Z"
              fill="url(#gh)"/>
            <path d="M256.2,333.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gi)"/>
            <path d="M256.2,326.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gj)"/>
            <path d="M256.2,319.4h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gk)"/>
            <path d="M256.2,312.3h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gl)"/>
            <path
              d="M269,304.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,266,309a7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,304.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,304.4Z"
              fill="url(#gm)"/>
            <path d="M256.2,293.1h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gn)"/>
            <path
              d="M269,285.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,285.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,285.1Z"
              fill="url(#go)"/>
            <path d="M256.2,273.8h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gp)"/>
            <path
              d="M269,265.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,265.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,265.8Z"
              fill="url(#gq)"/>
            <path d="M256.2,254.5h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gr)"/>
            <path
              d="M269,246.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,259,242a7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,269,246.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,267,246.6Z"
              fill="url(#gs)"/>
            <path
              d="M269,234.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,234.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,234.2Z"
              fill="url(#gt)"/>
            <path d="M256.2,222.9h12.6v2.3H258.2v2.7h-2Z"
                  fill="url(#gu)"/>
            <path
              d="M269,215a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,7.55,7.55,0,0,1-3.5.7,7.82,7.82,0,0,1-3.5-.7,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,7.55,7.55,0,0,1,3.5-.7,7.82,7.82,0,0,1,3.5.7,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,269,215Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,267,215Z"
              fill="url(#gv)"/>
            <path
              d="M355,344.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,344.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,344.8Z"
              fill="url(#gw)"/>
            <path d="M342.2,333.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#gx)"/>
            <path d="M342.2,326.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#gy)"/>
            <path d="M342.2,319.4h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#gz)"/>
            <path d="M342.2,312.3h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#ha)"/>
            <path
              d="M355,304.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,352,309a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,304.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,304.4Z"
              fill="url(#hb)"/>
            <path d="M342.2,293.1h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#hc)"/>
            <path
              d="M355,285.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,285.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,285.1Z"
              fill="url(#hd)"/>
            <path d="M342.2,273.8h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#he)"/>
            <path
              d="M355,265.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,265.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,265.8Z"
              fill="url(#hf)"/>
            <path d="M342.2,254.5h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#hg)"/>
            <path
              d="M355,246.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,345,242a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,355,246.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,353,246.6Z"
              fill="url(#hh)"/>
            <path
              d="M355,234.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,234.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,234.2Z"
              fill="url(#hi)"/>
            <path d="M342.2,222.9h12.6v2.3H344.2v2.7h-2Z"
                  fill="url(#hj)"/>
            <path
              d="M355,215a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,355,215Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,353,215Z"
              fill="url(#hk)"/>
            <path
              d="M472,344.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,344.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,344.8Z"
              fill="url(#hl)"/>
            <path d="M459.2,333.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hm)"/>
            <path d="M459.2,326.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hn)"/>
            <path d="M459.2,319.4h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ho)"/>
            <path d="M459.2,312.3h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hp)"/>
            <path
              d="M472,304.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,469,309a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,304.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,304.4Z"
              fill="url(#hq)"/>
            <path d="M459.2,293.1h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hr)"/>
            <path
              d="M472,285.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,285.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,285.1Z"
              fill="url(#hs)"/>
            <path d="M459.2,273.8h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#ht)"/>
            <path
              d="M472,265.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,265.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,265.8Z"
              fill="url(#hu)"/>
            <path d="M459.2,254.5h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hv)"/>
            <path
              d="M472,246.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,462,242a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,472,246.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,470,246.6Z"
              fill="url(#hw)"/>
            <path
              d="M472,234.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,234.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,234.2Z"
              fill="url(#hx)"/>
            <path d="M459.2,222.9h12.6v2.3H461.2v2.7h-2Z"
                  fill="url(#hy)"/>
            <path
              d="M472,215a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,472,215Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,470,215Z"
              fill="url(#hz)"/>
            <path
              d="M176.4,435.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,435.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,435.8Z"
              fill="url(#ia)"/>
            <path d="M163.6,424.5h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path d="M163.6,417.5h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path d="M163.6,410.4h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path d="M163.6,403.3h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,395.4a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,395.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,395.4Z"
              fill="url(#ib)"/>
            <path d="M163.6,384.1h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,376.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,376.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,376.1Z"
              fill="url(#ib)"/>
            <path d="M163.6,364.8h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,356.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,356.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,356.8Z"
              fill="url(#ib)"/>
            <path d="M163.6,345.5h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,337.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,176.4,337.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,174.4,337.6Z"
              fill="url(#ib)"/>
            <path
              d="M176.4,325.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,325.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,325.2Z"
              fill="url(#ib)"/>
            <path d="M163.6,313.9h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,306a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,306Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,306Z"
              fill="url(#ib)"/>
            <path d="M163.6,294.6h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,286.7a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,286.7Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,286.7Z"
              fill="url(#ib)"/>
            <path d="M163.6,275.4h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,267.4a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,267.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,267.4Z"
              fill="url(#ib)"/>
            <path
              d="M176.4,255.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,255.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,255.1Z"
              fill="url(#ib)"/>
            <path
              d="M176.4,242.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,242.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,242.8Z"
              fill="url(#ib)"/>
            <path d="M163.6,231.5h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,223.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,223.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,223.5Z"
              fill="url(#ib)"/>
            <path d="M163.6,212.2h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M176.4,204.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,176.4,204.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,174.4,204.2Z"
              fill="url(#ib)"/>
            <path d="M163.6,192.9h12.6v2.3H165.6v2.7h-2Z"
                  fill="url(#ib)"/>
            <path
              d="M206.4,502.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,502.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,502.8Z"
              fill="url(#ja)"/>
            <path d="M193.6,491.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jb)"/>
            <path d="M193.6,484.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jc)"/>
            <path d="M193.6,477.4h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jd)"/>
            <path d="M193.6,470.3h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#je)"/>
            <path
              d="M206.4,462.4a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,462.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,462.4Z"
              fill="url(#jf)"/>
            <path d="M193.6,451.1h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jg)"/>
            <path
              d="M206.4,443.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,443.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,443.1Z"
              fill="url(#jh)"/>
            <path d="M193.6,431.8h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#ji)"/>
            <path
              d="M206.4,423.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,423.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,423.8Z"
              fill="url(#jj)"/>
            <path d="M193.6,412.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jk)"/>
            <path
              d="M206.4,404.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,206.4,404.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,204.4,404.6Z"
              fill="url(#jl)"/>
            <path
              d="M206.4,392.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,392.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,392.2Z"
              fill="url(#jm)"/>
            <path d="M193.6,380.9h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jn)"/>
            <path
              d="M206.4,373a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,373Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,373Z"
              fill="url(#jo)"/>
            <path
              d="M206.4,344.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,344.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,344.8Z"
              fill="url(#jp)"/>
            <path d="M193.6,333.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jq)"/>
            <path d="M193.6,326.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jr)"/>
            <path d="M193.6,319.4h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#js)"/>
            <path d="M193.6,312.3h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jt)"/>
            <path
              d="M206.4,304.4a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,304.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,304.4Z"
              fill="url(#ju)"/>
            <path d="M193.6,293.1h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jv)"/>
            <path
              d="M206.4,285.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,285.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,285.1Z"
              fill="url(#jw)"/>
            <path d="M193.6,273.8h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jx)"/>
            <path
              d="M206.4,265.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,265.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,265.8Z"
              fill="url(#jy)"/>
            <path d="M193.6,254.5h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#jz)"/>
            <path
              d="M206.4,246.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,206.4,246.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,204.4,246.6Z"
              fill="url(#ka)"/>
            <path
              d="M206.4,234.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,234.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,234.2Z"
              fill="url(#kb)"/>
            <path d="M193.6,222.9h12.6v2.3H195.6v2.7h-2Z"
                  fill="url(#kc)"/>
            <path
              d="M206.4,215a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,206.4,215Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,204.4,215Z"
              fill="url(#kd)"/>
            <path
              d="M297,288.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,288.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,288.8Z"
              fill="url(#ke)"/>
            <path d="M284.2,277.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#kf)"/>
            <path d="M284.2,270.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#kg)"/>
            <path d="M284.2,263.4h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#kh)"/>
            <path d="M284.2,256.3h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#ki)"/>
            <path
              d="M297,248.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,294,253a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,248.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,248.4Z"
              fill="url(#kj)"/>
            <path d="M284.2,237.1h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#kk)"/>
            <path
              d="M297,229.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,229.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,229.1Z"
              fill="url(#kl)"/>
            <path d="M284.2,217.8h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#km)"/>
            <path
              d="M297,209.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,209.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,209.8Z"
              fill="url(#kn)"/>
            <path d="M284.2,198.5h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#ko)"/>
            <path
              d="M297,190.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,287,186a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A4.55,4.55,0,0,1,297,190.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.61,2.61,0,0,0,295,190.6Z"
              fill="url(#kp)"/>
            <path
              d="M297,178.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,178.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,178.2Z"
              fill="url(#kq)"/>
            <path d="M284.2,166.9h12.6v2.3H286.2v2.7h-2v-5Z"
                  fill="url(#kr)"/>
            <path
              d="M297,159a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,297,159Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,295,159Z"
              fill="url(#ks)"/>
            <path d="M370.2,289.7h12.6V292H372.2v2.7h-2Z"
                  fill="url(#kt)"/>
            <path
              d="M383,281.8a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,281.8Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,281.8Z"
              fill="url(#ku)"/>
            <path d="M370.2,270.5h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#kv)"/>
            <path
              d="M383,262.5a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,262.5Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,262.5Z"
              fill="url(#kw)"/>
            <path
              d="M383,250.2a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,250.2Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,250.2Z"
              fill="url(#kx)"/>
            <path d="M370.2,238.9h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#ky)"/>
            <path
              d="M383,230.9a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,230.9Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,230.9Z"
              fill="url(#kz)"/>
            <path
              d="M383,218.6a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4A5.18,5.18,0,0,1,373,214a9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,218.6Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,218.6Z"
              fill="url(#la)"/>
            <path d="M370.2,207.3h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#lb)"/>
            <path
              d="M383,199.3a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,199.3Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,199.3Z"
              fill="url(#lc)"/>
            <path d="M370.2,188h12.6v2.3H372.2V193h-2Z" fill="url(#ld)"/>
            <path
              d="M383,180a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,180Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,180Z"
              fill="url(#le)"/>
            <path
              d="M383,167.7a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,167.7Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,167.7Z"
              fill="url(#lf)"/>
            <path
              d="M383,155.4a5,5,0,0,1-.8,2.7A5.85,5.85,0,0,1,380,160a9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,155.4Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,155.4Z"
              fill="url(#lg)"/>
            <path d="M370.2,144.1h12.6v2.3H372.2v2.7h-2Z"
                  fill="url(#lh)"/>
            <path
              d="M383,136.1a5,5,0,0,1-.8,2.7,5.85,5.85,0,0,1-2.2,1.9,9.1,9.1,0,0,1-7,0,5.18,5.18,0,0,1-2.2-1.9,5,5,0,0,1,0-5.4,5.18,5.18,0,0,1,2.2-1.9,9.1,9.1,0,0,1,7,0,5.18,5.18,0,0,1,2.2,1.9A5,5,0,0,1,383,136.1Zm-2,0a2.48,2.48,0,0,0-1.1-2.1,5.94,5.94,0,0,0-3.3-.8,5.71,5.71,0,0,0-3.3.8,2.55,2.55,0,0,0,0,4.2,5.94,5.94,0,0,0,3.3.8,5.71,5.71,0,0,0,3.3-.8A2.48,2.48,0,0,0,381,136.1Z"
              fill="url(#li)"/>
          </g>

        </g>
      </g>
      <g class="site-box">
        <g class="site-background" opacity="0.8">
          <path
            d="M32,140H296a8,8,0,0,1,8,8V492a8,8,0,0,1-8,8H32a8,8,0,0,1-8-8V148A8,8,0,0,1,32,140Z"
            fill="url(#lm)"/>
          <path
            d="M32,139H296a9,9,0,0,1,9,9V492a9,9,0,0,1-9,9H32a9,9,0,0,1-9-9V148A9,9,0,0,1,32,139Z"
            fill="none"
            stroke-width="2" stroke="url(#ln)"/>
        </g>
        <g class="site-shapes">
          <path
            d="M48,362H242a4,4,0,0,1,4,4v4a4,4,0,0,1-4,4H48a4,4,0,0,1-4-4v-4A4,4,0,0,1,48,362Z"
            fill="#423b7e"
            fill-opacity="0.8"/>
          <path
            d="M48,390H199a4,4,0,0,1,4,4v4a4,4,0,0,1-4,4H48a4,4,0,0,1-4-4v-4A4,4,0,0,1,48,390Z"
            fill="#423b7e"
            fill-opacity="0.8"/>
          <path
            d="M48,418h72a4,4,0,0,1,4,4v24a4,4,0,0,1-4,4H48a4,4,0,0,1-4-4V422A4,4,0,0,1,48,418Z"
            opacity="0.8"
            fill="url(#lo)" style="isolation:isolate"/>
        </g>
        <g class="site-bar">
          <path d="M24,148a8,8,0,0,1,8-8H296a8,8,0,0,1,8,8v22H24Z"
                fill="url(#lp)"/>
          <circle cx="41" cy="155" r="5" fill="#fb762b"/>
          <circle cx="57" cy="155" r="5" fill="#32bba9"/>
          <circle cx="73" cy="155" r="5" fill="#a0cd37"/>
        </g>
        <path class="text-content-wrapper"
              d="M48,210H260a4,4,0,0,1,4,4V334a4,4,0,0,1-4,4H48a4,4,0,0,1-4-4V214A4,4,0,0,1,48,210Z"
              fill="#423b7e"/>
        <g class="site-text-wrapper" clip-path="url(#lq)">
          <path
            d="M74,234H224a4,4,0,0,1,4,4v4a4,4,0,0,1-4,4H74a4,4,0,0,1-4-4v-4A4,4,0,0,1,74,234Z"
            fill="url(#lr)"/>
          <path
            d="M74,258H188a4,4,0,0,1,4,4v4a4,4,0,0,1-4,4H74a4,4,0,0,1-4-4v-4A4,4,0,0,1,74,258Z"
            fill="url(#ls)"/>
          <path
            d="M74,282H224a4,4,0,0,1,4,4v4a4,4,0,0,1-4,4H74a4,4,0,0,1-4-4v-4A4,4,0,0,1,74,282Z"
            fill="url(#lt)"/>
          <path
            d="M74,306H215a4,4,0,0,1,4,4v4a4,4,0,0,1-4,4H74a4,4,0,0,1-4-4v-4A4,4,0,0,1,74,306Z"
            fill="url(#lu)"/>
          <path
            d="M74,329.2H224a4,4,0,0,1,4,4v4a4,4,0,0,1-4,4H74a4,4,0,0,1-4-4v-4A4,4,0,0,1,74,329.2Z"
            fill="url(#lv)"/>
          <path
            d="M74,353.2H188a4,4,0,0,1,4,4v4a4,4,0,0,1-4,4H74a4,4,0,0,1-4-4v-4A4,4,0,0,1,74,353.2Z"
            fill="url(#lw)"/>
          <path
            d="M74,377.2H224a4,4,0,0,1,4,4v4a4,4,0,0,1-4,4H74a4,4,0,0,1-4-4v-4A4,4,0,0,1,74,377.2Z"
            fill="url(#lx)"/>
          <path
            d="M74,401.2H215a4,4,0,0,1,4,4v4a4,4,0,0,1-4,4H74a4,4,0,0,1-4-4v-4A4,4,0,0,1,74,401.2Z"
            fill="url(#ly)"/>
        </g>
      </g>
      <g class="spotlight">
        <g class="glow-wrapper">
          <circle cx="500" cy="294.6" r="116" opacity="0.1" fill="url(#md)"
                  style="isolation:isolate"/>
          <g class="spin-1">

            <path
              d="M500.5,336.46a41.32,41.32,0,1,1,41.31-41.32,13.71,13.71,0,1,1-27.42,0A13.89,13.89,0,1,0,500.5,309a13.72,13.72,0,0,1,0,27.43Z"
              fill="url(#mf)"/>
            <path
              d="M528.5,309.44a13.7,13.7,0,1,0-13.7-13.7A13.66,13.66,0,0,0,528.5,309.44Z"
              fill="#a0cd37"/>
          </g>
          <g class="spin-2">
            <path
              d="M460,352.26a13.68,13.68,0,0,1-9.36-3.69A73.15,73.15,0,1,1,554,345,13.72,13.72,0,0,1,534,326.28a45.71,45.71,0,1,0-64.61,2.24A13.71,13.71,0,0,1,460,352.26Z"
              fill="url(#me)"/>
            <path
              d="M544.1,349.44a13.7,13.7,0,1,0-13.7-13.7A13.66,13.66,0,0,0,544.1,349.44Z"
              fill="#fb762b" opacity="0.8"
              style="isolation:isolate"/>
          </g>
          <g class="spin-3">
            <path
              d="M500,399.76a105.12,105.12,0,0,1,0-210.23A13.72,13.72,0,0,1,500,217a77.69,77.69,0,1,0,77.69,77.68,13.71,13.71,0,0,1,27.42,0A105.23,105.23,0,0,1,500,399.76Z"
              fill="url(#mg)"/>
            <path
              d="M500,216.84a13.7,13.7,0,1,0-13.7-13.7A13.66,13.66,0,0,0,500,216.84Z"
              fill="#32bba9" opacity="0.8"
              style="isolation:isolate"/>
          </g>
        </g>
        <g class="spotlight-base">
          <path
            d="M474,412a8,8,0,0,1,8-8h34a8,8,0,0,1,8,8v30a4,4,0,0,1-4,4H478a4,4,0,0,1-4-4Z"
            opacity="0.4" fill="url(#lz)"
            style="isolation:isolate"/>
          <path
            d="M469,412c0-4.4,3.3-8,7.4-8h45.1c4.1,0,7.4,3.6,7.4,8s-3.3,8-7.4,8H476.4C472.3,420,469,416.4,469,412Z"
            fill-rule="evenodd" opacity="0.8" fill="url(#ma)"
            style="isolation:isolate"/>
          <path d="M477,420a8,8,0,1,0-8-8A8,8,0,0,0,477,420Z"
                fill="#a0cd37" opacity="0.4" style="isolation:isolate"/>
          <path
            d="M469,432c0-4.4,3.3-8,7.4-8h45.1c4.1,0,7.4,3.6,7.4,8s-3.3,8-7.4,8H476.4C472.3,440,469,436.4,469,432Z"
            fill-rule="evenodd" opacity="0.8" fill="url(#mb)"
            style="isolation:isolate"/>
          <path d="M482,446h34v10a4,4,0,0,1-4,4H486a4,4,0,0,1-4-4Z"
                opacity="0.8" fill="url(#mc)" style="isolation:isolate"/>
          <path d="M477,440a8,8,0,1,0-8-8A8,8,0,0,0,477,440Z"
                fill="#a0cd37" opacity="0.4" style="isolation:isolate"/>
          <path d="M521,440a8,8,0,1,0-8-8A8,8,0,0,0,521,440Z"
                fill="#a0cd37" opacity="0.4" style="isolation:isolate"/>
          <path d="M521,420a8,8,0,1,0-8-8A8,8,0,0,0,521,420Z"
                fill="#a0cd37" opacity="0.4" style="isolation:isolate"/>
        </g>

      </g>
    </svg>
  </div>
<?php
