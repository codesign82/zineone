<?php
/****************************
 *     Custom ACF Meta      *
 ****************************/
$hero_version_2 = get_field( 'hero_version_2' );
$title          = @$hero_version_2['title'];
$page_name      = @$hero_version_2['page_name'];
$image          = @$hero_version_2['image'];
$text           = @$hero_version_2['text'];
$card_content   = @$hero_version_2['card_content'];
$is_logos       = @$card_content['is_logos'];
$card_text      = @$card_content['card_text'];
$card_text_info = @$card_content['card_text_info']; ?>
  <div class="left-hero-content">
    <?php if ( $page_name ) { ?>
      <h6 class="left-hero-content-text"><?= $page_name ?></h6>
    <?php } ?>
    <?php if ( $title ) { ?>
      <h2 class="headline-1"><?= $title ?></h2>
    <?php } ?>
    <?php if ( $image ) { ?>
      <picture class="left-hero-content-img aspect-ratio">
        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
      </picture>
    <?php } ?>
  </div>
  <div class="right-hero-content">
    <?php if ( $text ) { ?>
      <div class="paragraph description-p"><?= $text ?></div>
    <?php } ?>
    <?php if ( ! $is_logos ) { ?>
      <div class="hero-box box-shadow">
        <svg width="32" height="32" viewBox="0 0 32 32" fill="none">
          <path
            d="M15.1795 4.57143C13.265 6.17544 11.6581 8.18045 10.359 10.5865C9.05983 12.9925 8.34188 15.3183 8.20513 17.5639L13.5385 22.3759C13.4017 24.381 12.7521 26.2657 11.5897 28.0301C10.4274 29.7945 8.99145 31.1178 7.28205 32C2.42735 29.6742 0 25.9048 0 20.6917C0 16.8421 1.05983 13.0727 3.17949 9.38346C5.36752 5.61404 8.37607 2.48621 12.2051 0L15.1795 4.57143ZM32 4.57143C30.0855 6.17544 28.4786 8.18045 27.1795 10.5865C25.8803 12.9925 25.1624 15.3183 25.0256 17.5639L30.359 22.3759C30.2222 24.381 29.5727 26.2657 28.4103 28.0301C27.2479 29.7945 25.812 31.1178 24.1026 32C19.2479 29.6742 16.8205 25.9048 16.8205 20.6917C16.8205 16.8421 17.8803 13.0727 20 9.38346C22.188 5.61404 25.1966 2.48621 29.0256 0L32 4.57143Z"
            fill="#FB762B"/>
        </svg>
        <?php if ( $card_text ) { ?>
          <div class="paragraph box-text-1"><?= $card_text ?></div>
        <?php } ?>
        <?php if ( $card_text_info ) { ?>
          <div class="paragraph box-text-2"><?= $card_text_info ?></div>
        <?php } ?>
      </div>
    <?php } else { ?>
      <div class="hero-box success-box box-shadow">
        <?php if ( have_rows( 'hero_version_2' ) ): while ( have_rows( 'hero_version_2' ) ) :
          the_row();
          if ( have_rows( 'card_content' ) ): while ( have_rows( 'card_content' ) ) :
            the_row();
            if ( have_rows( 'logos' ) ): while ( have_rows( 'logos' ) ) :
              the_row();
              $logo_image = get_sub_field( 'logo_image' );
              $logo_url   = get_sub_field( 'logo_url' );
              ?>
              <div class="box-img">
                <?= $logo_url ? '<a href="' . $logo_url . '">' : '<picture>' ?>
                <img src="<?= $logo_image['url'] ?>" alt="<?= $logo_image['alt'] ?>">
                <?= $logo_url ? '</a>' : '</picture>' ?>
              </div>
            <?php endwhile;
            endif;
          endwhile;
          endif;
        endwhile;
        endif; ?>
      </div>
    <?php } ?>
  </div>
<?php
